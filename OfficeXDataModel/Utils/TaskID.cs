﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Utils
{
    public class TaskID
    {
        public enum eTaskEnum { eProjectTask, eAusence };

        private static Dictionary<eTaskEnum, String> s_Type2Str;
        private static Dictionary<String,eTaskEnum> s_Str2Type;

        /// <summary>
        /// 
        /// </summary>
        public Int32 ID
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public eTaskEnum TaskType
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="taskType"></param>
        public TaskID( Int32 id, eTaskEnum taskType )
        {
            this.ID = id;
            this.TaskType = taskType;
        }

        /// <summary>
        /// 
        /// </summary>
        private TaskID()
        {
        }

        /// <summary>
        /// Static constructor
        /// </summary>
        static TaskID()
        {
            s_Type2Str = new Dictionary<eTaskEnum, String>();
            s_Str2Type = new Dictionary<String, eTaskEnum>();

            s_Type2Str.Add( eTaskEnum.eProjectTask, "PT" );
            s_Str2Type.Add( "PT",eTaskEnum.eProjectTask );

            s_Type2Str.Add( eTaskEnum.eAusence, "AT" );
            s_Str2Type.Add( "AT", eTaskEnum.eAusence );

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format( "{0}-" + s_Type2Str[this.TaskType], this.ID );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strValue"></param>
        /// <param name="taskID"></param>
        /// <returns></returns>
        public static Boolean TryParse( String strValue, out TaskID taskID )
        {
            taskID = null;
            String[] tokens = strValue.Trim().ToUpper().Split( new Char[] { '-' } );

            if( tokens.Length == 2 )
            {
                tokens[0] = tokens[0].Trim();
                tokens[1] = tokens[1].Trim();

                if( s_Str2Type.ContainsKey( tokens[1] ) )
                {
                    Int32 id = 0;
                    if( Int32.TryParse( tokens[0], out id ) )
                    {
                        taskID = new TaskID( id, s_Str2Type[tokens[1]] );
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strValue"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public static Boolean TryParse( String strValue, out IReadOnlyCollection<TaskID> tasks )
        {
            List<TaskID> result = new List<TaskID>();
            tasks = result;

            String[] tasks_tokens = strValue.Trim().ToUpper().Split( new Char[] { ',' } );


            foreach( String task_str_id in tasks_tokens )
            {
                TaskID task_id = null;
                if( TaskID.TryParse( task_str_id, out task_id ) )
                {
                    result.Add( task_id );
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskIDs"></param>
        /// <returns></returns>
        public static String TaskIDs2Str( IEnumerable<TaskID> taskIDs )
        {
            StringBuilder sb = new StringBuilder();

            foreach( TaskID item in taskIDs )
            {
                if( sb.Length > 0 )
                {
                    sb.Append( ", " );
                }

                sb.Append( item.ToString() );
            }

            return sb.ToString();
        }
    }
}
