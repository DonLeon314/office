﻿using OfficeXDataModel.Interfaces;
using OfficeXDataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace OfficeXDataModel.Utils
{
    internal class ListItemCreator:
            ITaskVisitor
    {
        public ListItemCreator()
        {
        }

        public ListItem Result
        {
            get;
            private set;
        }
        #region interface ITaskVisitor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public Boolean Visit( ProjectTask task )
        {
            String value = new TaskID( task.Id, TaskID.eTaskEnum.eProjectTask ).ToString();

            String text = String.Format( "{0} {1}", task.Hours.ToString( "hh\\:mm" ), task.Description );

            this.Result = new ListItem( text, value );

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public Boolean Visit( AbsenceTask task )
        {
            String value = new TaskID( task.Id, TaskID.eTaskEnum.eAusence ).ToString();
            String text = task.Description;

            this.Result = new ListItem( text, value );

            switch( task.Status )
            {
                case AbsenceTask.eStatusAbsence.Request:
                    {
                        this.Result.Attributes.Add( "class", "request" );
                        break;
                    }
                case AbsenceTask.eStatusAbsence.Readed:
                    {   // No 
                        this.Result.Attributes.Add( "class", "readed" );
                        break;
                    }
                case AbsenceTask.eStatusAbsence.Approved:
                    {  // OK!
                        this.Result.Attributes.Add( "class", "approved" );
                        break;
                    }
            }

            return true;
        }
        #endregion
    }
}
