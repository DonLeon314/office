﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Utils
{
    public static class LINQEx
    {
        public static void ForEach<T>( this IEnumerable<T> _this, Action<T> action )
        {
            foreach( T item in _this )
            {
                action( item );
            }
        }
    }
}
