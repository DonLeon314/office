﻿using OfficeXDataModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace OfficeXDataModel.Utils
{
    public class TaskBuilder
    {
        

        public TaskBuilder()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOfficeXDBContext"></param>
        /// <param name="userID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ListItem> Build( IOfficeXDBContext iOfficeXDBContext, Int32 userID, DateTime date )
        {
            List<ITask> all_tasks = new List<ITask>();

            all_tasks.AddRange( LoadProjectTasks( iOfficeXDBContext, userID, date ) );

            all_tasks.AddRange( LoadAbsenceTasks( iOfficeXDBContext, userID, date ) );

            ListItemCreator item_creator = new ListItemCreator();
            
            List<ListItem> result = new List<ListItem>();

            foreach( ITask iTask in all_tasks )
            {
                if( iTask.Accept( item_creator ) )
                {
                    result.Add( item_creator.Result );
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOfficeXDBContext"></param>
        /// <param name="userID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        private IEnumerable<ITask> LoadProjectTasks( IOfficeXDBContext iOfficeXDBContext, Int32 userID, DateTime date )
        {
            return iOfficeXDBContext.GetProjectTaskByUserIDAndDate( userID, date );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iOfficeXDBContext"></param>
        /// <param name="userID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        private IEnumerable<ITask> LoadAbsenceTasks( IOfficeXDBContext iOfficeXDBContext, Int32 userID, DateTime date )
        {
            return iOfficeXDBContext.GetAbsenceTasksProjectTaskByUserIDAndDate( userID, date );
        }
    }
}
