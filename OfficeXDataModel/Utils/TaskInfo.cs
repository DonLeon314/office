﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Utils
{
    public class TaskInfo
    {
        public String Text
        {
            get;
            private set;
        }

        public TaskID TaskID
        {
            get;
            private set;
        }

        public TaskInfo( Int32 id, String text, TaskID.eTaskEnum taskType )
        {
            this.Text = text;
            this.TaskID = new TaskID( id, taskType );
        }
    }
}
