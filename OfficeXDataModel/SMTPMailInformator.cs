﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Net;
using System.Diagnostics;
using System.IO;

namespace OfficeX
{
    public class SMTPMailInformator
    {
        private String _Host = "smtp.google.com";
        private Int32 _Port = 587;
        private String _User = "donleon413@gmail.com";
        private String _Password = "tortilla1971";


        /// <summary>
        /// 
        /// </summary>
        private SMTPMailInformator()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static SMTPMailInformator Create()
        {
            SMTPMailInformator result = new SMTPMailInformator();

            result.Initialize();

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Initialize()
        {
            this._Host = "mail.mindden.com";
            this._Port = 587;
            this._User = "rrhh@mindden.com";
            this._Password = "RRHHMindden2018";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="a"></param>
        public void OnSend( Object sender, InformatorArgs a )
        {
            SendNotify( a.ToUser, a.BodyText, a.TitleText, a.IsBodyHtml );
        }
              

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mailTo"></param>
        /// <param name="textBody"></param>
        /// <param name="textSubj"></param>
        public void SendNotify( String mailTo, String textBody, String textSubj = null, Boolean isBodyHtml = false )
        {            

            try
            {
                var client = new SmtpClient( this._Host, this._Port );

                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential( this._User, this._Password );

                MailMessage message = new MailMessage()
                {
                    From = new System.Net.Mail.MailAddress( this._User ),                    
                    Subject = textSubj,
                    Body = textBody                
                };

                message.IsBodyHtml = isBodyHtml;

                message.To.Add( new System.Net.Mail.MailAddress( mailTo ) );

                client.Send( message );
                
            }
            catch( Exception ex )
            {
                Debug.WriteLine( "MailNotifyInformator error: " + ex.Message );
            }
        }
    }
}