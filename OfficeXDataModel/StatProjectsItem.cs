﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OfficeX.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class StatProjectsItem
    {
        /// <summary>
        /// User name
        /// </summary>
        public String UserName
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public TimeSpan Hours
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="hours"></param>
        public StatProjectsItem( String userName, TimeSpan hours )
        {
            this.Hours = hours;
            this.UserName = userName;
        }
    }
}