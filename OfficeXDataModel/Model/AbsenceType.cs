﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Model
{
    public class AbsenceType
    {
        public Int32 id
        {
            get;
            set;
        }

        public String Name
        {
            get;
            set;
        }

        public enum eParte
        {
            FirstParte = 0, // 0
            SecondParte,    // 1
            AllDay          // 2           
        }

        [DefaultValue(eParte.FirstParte)]
        public eParte Duration
        {
            get;
            set;
        }
    }
}
