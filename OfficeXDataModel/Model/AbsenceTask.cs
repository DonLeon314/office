﻿using OfficeXDataModel.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Model
{
    public class AbsenceTask:
            ITask
    {
        public Int32 Id
        {
            get;
            set;
        }

        public Int32 UserProfileId
        {
            get;
            set;
        }

        public UserProfile UserProfile
        {
            get;
            set;
        }

        [Column( TypeName = "date" )]
        public DateTime StartDate
        {
            get;
            set;
        }

        [Column( TypeName = "date" )]
        public DateTime? EndDate
        {
            get;
            set;
        }

        public String Description
        {
            get;
            set;
        }
                
        public enum eStatusAbsence
        {
            Request,
            Readed,
            Approved
        }
        public eStatusAbsence Status
        {
            get;
            set;
        }

        public Int32 AbsenceTypeId
        {
            get;
            set;
        }

        public AbsenceType AbsenceType
        {
            get;
            set;
        }

        #region interface ITask

        public Boolean Accept( ITaskVisitor visitor )
        {
            return visitor.Visit( this );
        }

        #endregion
    }
}
