﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class SharedFile
    {
        /// <summary>
        /// 
        /// </summary>
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 SharedFileFolderId
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public SharedFile SharedFileFolder
        {
            get;
            set;
        }
    }
}
