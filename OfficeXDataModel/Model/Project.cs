﻿using OfficeXDataModel.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace OfficeXDataModel.Model
{
    public class Project:
            IProjectContextDataSource
    {
        public Int32 Id
        {
            get;
            set;
        }

        [Required]
        [MaxLength(50)]
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<UserProfile> UserProfiles
        {
            get;
            set;
        }

        /// <summary>
        /// Contact
        /// </summary>
        [MaxLength( 50 )]
        public String ProjectContactName
        {
            get;
            set;
        }

        /// <summary>
        /// Contact e-mail
        /// </summary>
        [MaxLength( 30 )]
        public String ProjectContactEMail
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Project()
        {
            this.UserProfiles = new List<UserProfile>();
        }        
    }
}
