﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeXDataModel.Interfaces;

namespace OfficeXDataModel.Model
{
    public class ProjectTask:
                ITask
    {
        public Int32 Id
        {
            get;
            set;
        }

        public Int32 UserProfileId
        {
            get;
            set;
        }

        public UserProfile UserProfile
        {
            get;
            set;
        }

        public Int32 ProjectId
        {
            get;
            set;
        }

        public Project Project
        {
            get;
            set;
        }

        [Column( TypeName = "Date" )]
        public DateTime Date
        {
            get;
            set;
        }
                
        public TimeSpan Hours
        {
            get;
            set;
        }

        [MaxLength( 256 )]
        [DefaultValue("")]
        public String Description
        {
            get;
            set;
        }

        #region interface ITask

        public Boolean Accept( ITaskVisitor visitor )
        {
            return visitor.Visit( this );
        }

        #endregion
    }
}
