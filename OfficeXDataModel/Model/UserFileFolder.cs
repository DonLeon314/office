﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Model
{
    public class UserFileFolder
    {
        /// <summary>
        /// 
        /// </summary>
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 UserProfileId
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public UserProfile UserProfile
        {
            get;
            set;
        }

        public ICollection<UserFile> Files
        {
            get;
            set;
        }

        /// <summary>
        /// Constructor 
        /// </summary>
        public UserFileFolder()
        {
            this.Files = new List<UserFile>();
        }
    }
}
