﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Model
{
    /// <summary>
    /// 
    /// </summary>
    internal static class UserProfileEx
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_this"></param>
        /// <returns></returns>
        public static String UIName( this UserProfile _this )
        {
            StringBuilder sb = new StringBuilder();

            if( String.IsNullOrWhiteSpace( _this.Name ) == false )
            {
                sb.Append( _this.Name );
            }

            if( String.IsNullOrWhiteSpace( _this.SurName ) == false )
            {
                if( sb.Length > 0 )
                {
                    sb.Append( " " );
                }

                sb.Append( _this.SurName );
            }

            if( sb.Length == 0 )
            {
                sb.Append( _this.EMail );
            }

            return sb.ToString();
        }
    }
}
