﻿using OfficeXDataModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Model
{
    public class UserProfile:
        IUserProfileContextDataSource
    {
        public enum eStatus
        {
            Admin,
            User
        }

        public Int32 Id
        {
            get;
            set;
        }

        public String Name    // 1
        {
            get;
            set;
        }

        public String SurName   // 2
        {
            get;
            set;
        }

        public eStatus Status
        {
            get;
            set;
        }

        public String Address // 3
        {
            get;
            set;
        }

        public String Password
        {
            get;
            set;
        }

        #region Contacts
        public String EMail  // 10
        {
            get;
            set;
        }

        public String EMailPrivate  // 9
        {
            get;
            set;
        }

        public DateTime BirthDay  // 6
        {
            get;
            set;
        }

        public String Phone   // 4
        {
            get;
            set;
        }

        public String PhoneWork  // 5
        {
            get;
            set;
        }

        public ICollection<Project> Projects
        {
            get;
            set;
        }

        #endregion

        public DateTime ContractDate // 11
        {
            get;
            set;
        }

        #region Documents
        public String DocumentNum   // 7
        {
            get;
            set;
        }

        public String MedicalNum   // 8
        {
            get;
            set;
        }
                
        #endregion

        public Int32 ParentUserProfileId
        {
            get;
            set;
        }


        ICollection<UserFileFolder> FileFolders
        {
            get;
            set;
        }


        /// <summary>
        /// Constructor
        /// </summary>
        public UserProfile()
        {
            this.Projects = new List<Project>();            
        }
    }
}
