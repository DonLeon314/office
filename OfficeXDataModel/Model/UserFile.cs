﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class UserFile
    {
        /// <summary>
        /// 
        /// </summary>
        public Int32 Id
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 UserFileFolderId
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public UserFileFolder Folder
        {
            get;
            set;
        }
    }
}
