﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace OfficeXDataModel.Interfaces
{
    public interface IProjectContextDataSource
    {
        Int32 Id
        {
            get;
        }

        String Name
        {
            get;
        }

        String ProjectContactName
        {
            get;
        }

        String ProjectContactEMail
        {
            get;
        }        
    }
}
