﻿using OfficeXDataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Interfaces
{
    public interface IUserProfileUIDataSource
    {
        Int32 Id
        {
            get;
        }

        String UserName  // 1
        {
            get;
        }

        String Surname  // 2
        {
            get;
        }

        String Address // 3
        {
            get;
        }

        String Phone // 4
        {
            get;
        }

        String PhoneWork // 5
        {
            get;
        }

        DateTime BirthDay // 6
        {
            get;
        }

        String DocumentNum // 7
        {
            get;
        }

        String MedicalNum // 8
        {
            get;
        }

        String EMailPrivate // 9
        {
            get;
        }

        String EMail  // 10
        {
            get;
        }

        DateTime ContractDate // 11
        {
            get;
        }

        UserProfile.eStatus? Status
        {
            get;
        }
    }
}
