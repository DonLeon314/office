﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace OfficeXDataModel.Interfaces
{
    public interface IProjectTaskUIDataSource
    {
        Int32 ProjectTaskID
        {
            get;            
        }

        Int32 UserProfileId
        {
            get;
        }

        Int32 ProjectId
        {
            get;
        }

        String Description
        {
            get;
        }

        TimeSpan Hours
        {
            get;
        }

        DateTime Date
        {
            get;
        }
    }
}
