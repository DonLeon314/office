﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using OfficeX.Models;
using OfficeXDataModel.Model;
using OfficeXDataModel.Utils;
using static OfficeXDataModel.Model.AbsenceTask;

namespace OfficeXDataModel.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IOfficeXDBContext
    {

        List<StatProjectsItem> GetStatProjects( Int32 projectId, DateTime startDate, DateTime endDate );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserProfile GetUserByID( Int32 userId );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserProfileId"></param>
        void DeleteUserProfileById( Int32 userProfileId );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IUserProfileContextDataSource GetUserProfileByID( Int32 userId );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentUserProfileId"></param>
        /// <returns></returns>
        IReadOnlyCollection<ListItem> GetChildUsers( Int32 parentUserProfileId, Boolean AddSelf = false );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentUserProfileId"></param>
        /// <param name="AddSelf"></param>
        /// <returns></returns>
        IReadOnlyCollection<IUserProfileContextDataSource> GetChildUsersInfo( Int32 parentUserProfileId, Boolean AddSelf = false );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileId"></param>
        /// <returns></returns>
        String GetEmailByUserId( Int32 userProfileId );
                        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eMail"></param>
        /// <returns></returns>
        UserProfile GetUserByEmail( String eMail );
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="iUserProfileSource"></param>
        void SaveUser( IUserProfileUIDataSource iUserProfileSource, Int32 parentUserProfileId );


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iAbsenceTaskUIDataSource"></param>
        void SaveAbsenceTask( IAbsenceTaskUIDataSource iAbsenceTaskUIDataSource );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        Boolean ChangePassword( Int32 userId, String oldPassword, String newPassword );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Boolean AdminChangePassword( Int32 userId, String password );
                

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IReadOnlyCollection<Project> GetProjects();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        IProjectContextDataSource GetProjectByID( Int32 projectId );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iProjectUIDataSource"></param>
        void SaveProject( IProjectUIDataSource iProjectUIDataSource );

        /// <summary>
        /// Delete project by ID
        /// </summary>
        /// <param name="projectId"></param>
        void DeleteProjectById( Int32 projectId );
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        IReadOnlyCollection<Int32> GetProjectUsers( Int32 projectId );

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IReadOnlyCollection<UserProfile> GetAllUsers();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IReadOnlyCollection<ListItem> LoadAllUserItems();

        /// <summary>
        /// 
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        IReadOnlyCollection<ListItem> GetFoldersByUserId( Int32 userProfileId );

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IReadOnlyCollection<ListItem> GetSharedFolders();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        IReadOnlyCollection<ListItem> GetFilesByFolderId( Int32 folderId );
            
        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        IReadOnlyCollection<ListItem> GetSharedFilesByFolderId( Int32 folderId );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="folderId"></param>
        /// <param name="fileName"></param>
        void SaveFileToUserFolder( Int32 folderId, String fileName );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="fileName"></param>
        void SaveFileToSharedFolder( Int32 folderId, String fileName );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileID"></param>
        /// <returns></returns>
        IReadOnlyCollection<ListItem> GetProjectsByUserID( Int32 userProfileID );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        IReadOnlyCollection<ITask> GetProjectTaskByUserIDAndDate( Int32 userProfileID, DateTime date );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        IReadOnlyCollection<ITask> GetAbsenceTasksProjectTaskByUserIDAndDate( Int32 userProfileID, DateTime date );


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iProjectTaskUIDataSource"></param>
        void SaveProjectTask( IProjectTaskUIDataSource iProjectTaskUIDataSource );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tasks"></param>
        void DeleteTasks( IReadOnlyCollection<TaskID> tasks );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectTaskId"></param>
        /// <returns></returns>
        ProjectTask GetProjectTaskById( Int32 projectTaskId );


        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        IReadOnlyCollection<AbsenceTask> GetNewAbsences( DateTime date );

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="newStatus"></param>
        void AbsencesChangeStatus( IReadOnlyCollection<Int32> ids, eStatusAbsence newStatus );

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IReadOnlyCollection<ListItem> GetAbsenceTypes();

        /// <summary>
        /// Get AbsenceTask by ID
        /// </summary>
        /// <param name="absenceId"></param>
        /// <returns></returns>
        AbsenceTask GetAbsenceTaskById( Int32 absenceId );

    }
}
