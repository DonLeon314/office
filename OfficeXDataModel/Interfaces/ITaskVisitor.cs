﻿using OfficeXDataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Interfaces
{
    public interface ITaskVisitor
    {
        Boolean Visit( ProjectTask task );
        Boolean Visit( AbsenceTask task );
    }
}
