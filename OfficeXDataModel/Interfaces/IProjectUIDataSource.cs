﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Interfaces
{
    public interface IProjectUIDataSource
    {
        /// <summary>
        /// 
        /// </summary>
        UIDataItem<String> ProjectName
        {
            get;
        }

        /// <summary>
        /// 
        /// </summary>
        UIDataItem<Int32> Id
        {
            get;
        }

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<Int32> UsersChild
        {
            get;
        }

        /// <summary>
        /// 
        /// </summary>
        String ProjectContactName
        {
            get;
        }

        /// <summary>
        /// 
        /// </summary>
        String ProjectContactEMail
        {
            get;
        }
    }
}
