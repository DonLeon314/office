﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel.Interfaces
{
    public interface IPasswordUIDataSource
    {
        Int32 Id
        {
            get;
        }

        String Password
        {
            get;
        }
    }
}
