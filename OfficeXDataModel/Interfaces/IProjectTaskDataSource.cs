﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace OfficeXDataModel.Interfaces
{
    public interface IProjectTaskDataSource
    {
        Int32 ProjectTaskID
        {
            get;
        }

        Int32 UserProfileID
        {
            get;
        }

        Int32 ProjectID
        {
            get;
        }

        TimeSpan Hours
        {
            get;
        }

        String Description
        {
            get;
        }

        DateTime Date
        {
            get;
        }

        IReadOnlyCollection<ListItem> Projects
        {
            get;
        }
    }
}
