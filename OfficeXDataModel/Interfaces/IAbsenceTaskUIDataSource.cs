﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static OfficeXDataModel.Model.AbsenceTask;

namespace OfficeXDataModel.Interfaces
{
    public interface IAbsenceTaskUIDataSource
    {
        Int32 Id
        {
            get;
        }

        String Description
        {
            get;
        }

        Int32 UserProfileId
        {
            get;
        }

        DateTime StartDate
        {
            get;
        }

        DateTime? EndDate
        {
            get;
        }

        eStatusAbsence Status
        {
            get;
        }

        Int32 AbsenceTypeId
        {
            get;
        }
    }
}
