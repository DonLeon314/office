﻿using OfficeXDataModel.Interfaces;
using OfficeXDataModel.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel
{
    public class OfficeXDBContext:
                 DbContext
    {
        /// <summary>
        /// 
        /// </summary>
        public OfficeXDBContext():
               base( "OfficeSQLConnectionString" )
              // base("HomeSQLConnectionString")
        {
           // Database.SetInitializer<OfficeXDataModel>( new CreateDatabaseIfNotExists<OfficeXDBContext>() );
            Database.SetInitializer( new MigrateDatabaseToLatestVersion<OfficeXDBContext, OfficeXDataModel.Migrations.Configuration>() );
        }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<UserProfile> UserProfiles
        {
            get;
            set;
        }

        public DbSet<Project> Projects
        {
            get;
            set;
        }

        /// <summary>
        /// Project tasks
        /// </summary>
        public DbSet<ProjectTask> ProjectTasks
        {
            get;
            set;
        }

        /// <summary>
        /// Absence tasks
        /// </summary>
        public DbSet<AbsenceTask> AbsenceTasks
        {
            get;
            set;
        }

        /// <summary>
        /// Events
        /// </summary>
        public DbSet<EventInfo> Events
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<AbsenceType> AbsenceTypes
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<UserFileFolder> UserFileFolders
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<UserFile> UserFiles
        {
            get;
            set;
        }

        public DbSet<SharedFile> SharedFiles
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<SharedFileFolder> SharedFileFolders
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating( DbModelBuilder modelBuilder )
        {
            base.OnModelCreating( modelBuilder );
        }
    }
}
