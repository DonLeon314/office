﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OfficeX
{
    /// <summary>
    /// Informator event argument
    /// </summary>
    public class InformatorArgs:
            EventArgs
    {
        /// <summary>
        /// User destination
        /// </summary>
        public String ToUser
        { 
            get;
            private set;
        }

        /// <summary>
        /// Body text
        /// </summary>
        public String BodyText
        {
            get;
            private set;
        }

        /// <summary>
        /// Title text
        /// </summary>
        public String TitleText
        {
            get;
            private set;
        }

        /// <summary>
        /// Is body HTML
        /// </summary>
        public Boolean IsBodyHtml
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="toUser">User destination</param>
        /// <param name="bodyText">Body text</param>
        /// <param name="titleText">Title text</param>
        public InformatorArgs( String toUser, String bodyText, Boolean isBodyHtml = false, String titleText = null )
        {
            this.BodyText = bodyText;
            this.TitleText = titleText;
            this.ToUser = toUser;
            this.IsBodyHtml = isBodyHtml;
        }
    }
}