﻿using OfficeX;
using OfficeX.Models;
using OfficeXDataModel.Interfaces;
using OfficeXDataModel.Model;
using OfficeXDataModel.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using static OfficeXDataModel.Model.AbsenceTask;

namespace OfficeXDataModel
{
    public class OfficeXContext:
                IOfficeXDBContext
    {
        private OfficeXDBContext _DBContext = null;
        private SMTPMailInformator _Informator = SMTPMailInformator.Create();
                
        /// <summary>
        /// Constructor
        /// </summary>
        public OfficeXContext()
        {

            this._DBContext = new OfficeXDBContext();

            this.OnSend += this._Informator.OnSend;

        }

        public EventHandler<InformatorArgs> OnSend;


        #region interface IOfficeXDBContext

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentUserProfileId"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ListItem> GetChildUsers( Int32 parentUserProfileId, Boolean AddSelf = false )
        {            

            var users = this._DBContext.UserProfiles.Where( u => ( u.ParentUserProfileId == parentUserProfileId || 
                                                                   ( AddSelf && u.Id == parentUserProfileId ) ) ).
                                                     ToList();

            return users.Select( item => new ListItem( item.UIName(), item.Id.ToString() ) ).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentUserProfileId"></param>
        /// <param name="AddSelf"></param>
        /// <returns></returns>
        public IReadOnlyCollection<IUserProfileContextDataSource> GetChildUsersInfo( Int32 parentUserProfileId, Boolean AddSelf = false )
        {
            var users = this._DBContext.UserProfiles.Where( u => ( u.ParentUserProfileId == parentUserProfileId ||
                                                                       ( AddSelf && u.Id == parentUserProfileId ) ) ).
                                                         ToList();

            return users.Select( item => (IUserProfileContextDataSource)item ).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileId"></param>
        /// <returns></returns>
        public String GetEmailByUserId( Int32 userProfileId )
        {
            String result = String.Empty;

            var user_profile = this._DBContext.UserProfiles.Where( up => up.Id == userProfileId ).FirstOrDefault();

            if( user_profile != null )
            {
                result = user_profile.EMail;
            }

            return result;
        }


        public void DeleteUserProfileById( Int32 userProfileId )
        {
            UserProfile user_profile = new UserProfile() { Id = userProfileId };
            this._DBContext.UserProfiles.Attach( user_profile );
            this._DBContext.UserProfiles.Remove( user_profile );
            this._DBContext.SaveChanges();
        }

        /// <summary>
        /// Delete project by Id
        /// </summary>
        /// <param name="projectId"></param>
        public void DeleteProjectById( Int32 projectId )
        {
            Project project = new Project() { Id = projectId };
            this._DBContext.Projects.Attach( project );
            this._DBContext.Projects.Remove( project );
            this._DBContext.SaveChanges();

        }

        /// <summary>
        /// Get project by ID
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IProjectContextDataSource GetProjectByID( Int32 projectId )
        {
            Project result = this._DBContext.Projects.Where( proj => proj.Id == projectId ).FirstOrDefault();

            return result;
        }        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iProjectUIDataSource"></param>
        public void SaveProject( IProjectUIDataSource iProjectUIDataSource )
        {
            if( iProjectUIDataSource == null )
            {
                return;
            }

            var id_src = iProjectUIDataSource.Id;
            var name_src = iProjectUIDataSource.ProjectName;

            Int32 id = ( id_src.HasValue ? id_src.Value : 0 );
            String name = ( name_src.HasValue ? name_src.Value : String.Empty );

            if( String.IsNullOrWhiteSpace( name ) )
            {
                name = Guid.NewGuid().ToString();
            }

            Project project = null;
            if( id == 0 )
            {
                project = new Project()
                { 
                    Name = name,
                    ProjectContactName = iProjectUIDataSource.ProjectContactName,
                    ProjectContactEMail = iProjectUIDataSource.ProjectContactEMail
                };
                this._DBContext.Projects.Add( project );
            }
            else
            {

                project = this._DBContext.Projects.Include("UserProfiles").Where( proj => proj.Id == id ).FirstOrDefault();
                if( project == null )
                {
                    return; //TODO:WARNING!!!!
                }

                project.Name = name;

                project.ProjectContactName = iProjectUIDataSource.ProjectContactName;
                project.ProjectContactEMail = iProjectUIDataSource.ProjectContactEMail;
            }

            List<InformatorArgs> infos = new List<InformatorArgs>();

            // Delete users
            List<UserProfile> for_operation = new List<UserProfile>();

            foreach( var user_profile in project.UserProfiles )
            {
                if( false == iProjectUIDataSource.UsersChild.Contains( user_profile.Id ) )
                {
                    for_operation.Add( user_profile );
                }
            }

            foreach( UserProfile user_profile in for_operation )
            {
                project.UserProfiles.Remove( user_profile );

                infos.Add( new InformatorArgs( user_profile.EMail, 
                                               String.Format("El usuario:{0} ha sido excluido del proyecto:{1}",
                                                              user_profile.UIName(), project.Name ), false, "Cambios en el proyecto" ) );
            }

            for_operation.Clear();
            
            // Add users
            foreach( Int32 user_id in iProjectUIDataSource.UsersChild )
            {
                UserProfile user_profile = project.UserProfiles.Where( up => up.Id == user_id ).FirstOrDefault();
                if( user_profile == null )
                {
                    user_profile = this._DBContext.UserProfiles.Where( u => u.Id == user_id ).FirstOrDefault();
                    if( user_profile != null )
                    {
                        for_operation.Add( user_profile );
                        
                    }
                }
            }

            foreach( UserProfile user_profile in for_operation )
            {
                project.UserProfiles.Add( user_profile );
                infos.Add( new InformatorArgs( user_profile.EMail,
                                               String.Format( "El usuario:{0} ha sido incluido en el proyecto:{1}",
                                               user_profile.UIName(), project.Name ), false, "Nuevo proyecto asignado" ) );
            }

            this._DBContext.SaveChanges();

            // Send info to users
            foreach( var info in infos )
            {
                this.OnSend( this, info );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<Project> GetProjects()
        {
            return this._DBContext.Projects.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<StatProjectsItem> GetStatProjects( Int32 projectId, DateTime startDate, DateTime endDate )
        {

            var query = this._DBContext.ProjectTasks.Include( "UserProfile" ).
                                        Where( p => ( p.ProjectId == projectId && ( p.Date >= startDate && p.Date <= endDate ) ) ).
                                        ToList().
                                        GroupBy( p1 => p1.UserProfileId ).
                                        ToList();

            return query.Select( group => new StatProjectsItem( group.First().UserProfile.UIName(),
                         TimeSpan.FromMinutes( group.Sum( group_item => group_item.Hours.TotalMinutes ) ) ) ).
                         ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserProfile GetUserByID( Int32 userId )
        {

            UserProfile result = this._DBContext.UserProfiles.Where( up => up.Id == userId ).FirstOrDefault();

            return result;
        }

        public IUserProfileContextDataSource GetUserProfileByID( Int32 userId )
        {
            UserProfile result = this._DBContext.UserProfiles.Where( up => up.Id == userId ).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eMail"></param>
        /// <returns></returns>
        public UserProfile GetUserByEmail( String eMail )        
        {
            eMail = eMail.Trim();
            UserProfile result = this._DBContext.UserProfiles.Where( up => String.Compare( up.EMail, eMail, true ) == 0 ).
                                                              FirstOrDefault();

            return result;
        }

        //         /// <summary>
        //         /// Load child users
        //         /// </summary>
        //         /// <param name="parentUserProfileId"></param>
        //         /// <returns></returns>
        //         public IReadOnlyCollection<UserProfile> GetChildUsers( Int32 parentUserProfileId )
        //         {
        //             return this._DBContext.UserProfiles.Where( up => up.ParentUserProfileId == parentUserProfileId ).ToList();
        //         }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IReadOnlyCollection<Int32> GetProjectUsers( Int32 projectId )
        {
            Project project = this._DBContext.Projects.Include("UserProfiles").Where( proj => proj.Id == projectId ).FirstOrDefault();

            if( project == null )
            {
                return new List<Int32>();
            }

            return project.UserProfiles.Select( up => up.Id ).ToList(); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<UserProfile> GetAllUsers()
        {
            return this._DBContext.UserProfiles.ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<ListItem> LoadAllUserItems()
        {
            var users = this._DBContext.UserProfiles.ToList();
            List<ListItem> result = new List<ListItem>();

            foreach( var user in users )
            {
                String value = user.Id.ToString();
                StringBuilder text = new StringBuilder();

                if( String.IsNullOrWhiteSpace( user.Name ) == false )
                {
                    text.Append( user.Name );
                }

                if( String.IsNullOrWhiteSpace( user.SurName ) == false )
                {
                    if( text.Length != 0 )
                    {
                        text.Append( " " );
                    }
                    text.Append( user.SurName );
                }

                if( text.Length == 0 )
                {
                    text.Append( user.EMail );
                }

                result.Add( new ListItem( text.ToString(), value ) );
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileId"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ListItem> GetFoldersByUserId( Int32 userProfileId )
        {
            List<ListItem> result = new List<ListItem>();

            foreach( var folder in this._DBContext.UserFileFolders.Where( item => item.UserProfileId == userProfileId ).ToList() )
            {
                String text = folder.Name;
                String value = folder.Id.ToString();

                result.Add( new ListItem( text, value ) );
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<ListItem> GetSharedFolders()
        {
            var shared_folders = this._DBContext.SharedFileFolders.ToList();

            return shared_folders.Select( item => new ListItem( item.Name, item.Id.ToString() ) ).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="folderId"></param>
        /// <param name="fileName"></param>
        public void SaveFileToUserFolder( Int32 folderId, String fileName )
        {
            var file = this._DBContext.UserFiles.
                            Where( f => f.UserFileFolderId == folderId && String.Compare( fileName, f.Name, true ) == 0 ).FirstOrDefault();

            String message_mask = String.Empty;
            
            if( file == null )
            {
                UserFile user_file = new UserFile();
                user_file.UserFileFolderId = folderId;
                user_file.Name = fileName;
                
                this._DBContext.UserFiles.Add( user_file );

                this._DBContext.SaveChanges();

                message_mask = "El Archivo:{0} se ha añadido a la carpeta:{1}";
            }
            else
            {
                message_mask = "El archivo :{0} ha sido actualizado en la carpeta:{1}";
            }

            UserFileFolder folder = this._DBContext.UserFileFolders.Where( f => f.Id == folderId ).FirstOrDefault();
            UserProfile user_profile = GetFolderOwner( folderId );
            
            if( user_profile != null )
            {
                this.OnSend( this, new InformatorArgs( user_profile.EMail,
                                                       String.Format( message_mask, fileName, ( folder != null ? folder.Name : "" ) )
                                                      ) );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="fileName"></param>
        public void SaveFileToSharedFolder( Int32 folderId, String fileName )
        {
            var file = this._DBContext.SharedFiles.
                                Where( f => f.SharedFileFolderId == folderId && String.Compare( fileName, f.Name, true ) == 0 ).FirstOrDefault();

            String message_mask = String.Empty;

            if( file == null )
            {
                SharedFile shared_file = new SharedFile();
                shared_file.SharedFileFolderId = folderId;
                shared_file.Name = fileName;

                this._DBContext.SharedFiles.Add( shared_file );

                this._DBContext.SaveChanges();

                message_mask = "El Archivo:{0} se ha añadido a la carpeta:{1}";
            }
            else
            {
                message_mask = "El archivo :{0} ha sido actualizado en la carpeta:{1}";
            }

            SharedFileFolder folder = this._DBContext.SharedFileFolders.Where( f => f.Id == folderId ).FirstOrDefault();

            foreach( UserProfile user_profile in this._DBContext.UserProfiles.ToList() )
            {
                if( user_profile != null )
                {
                    this.OnSend( this, new InformatorArgs( user_profile.EMail,
                                                           String.Format( message_mask, fileName, ( folder != null ? folder.Name : "" ) )
                                                          , false, "Gestor documentos") );
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ListItem> GetFilesByFolderId( Int32 folderId )
        {
            List<ListItem> result = new List<ListItem>();

            foreach( var file in this._DBContext.UserFiles.Where( item => item.UserFileFolderId == folderId ).ToList() )
            {
                String text = file.Name;
                String value = file.Id.ToString();

                result.Add( new ListItem( text, value ) );
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ListItem> GetSharedFilesByFolderId( Int32 folderId )
        {
            List<ListItem> result = new List<ListItem>();

            foreach( var file in this._DBContext.SharedFiles.Where( item => item.SharedFileFolderId == folderId ).ToList() )
            {
                String text = file.Name;
                String value = file.Id.ToString();

                result.Add( new ListItem( text, value ) );
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iAbsenceTaskUIDataSource"></param>
        public void SaveAbsenceTask( IAbsenceTaskUIDataSource iAbsenceTaskUIDataSource )
        {
            AbsenceTask absence_task = null;

            if( iAbsenceTaskUIDataSource.Id == 0 )
            {
                absence_task = new AbsenceTask();
            }
            else
            {
                absence_task = GetAbsenceTaskById( iAbsenceTaskUIDataSource.Id );
                if( absence_task == null )
                {
                    //TODO: Show Error
                    return;
                }
            }

            absence_task.AbsenceTypeId = iAbsenceTaskUIDataSource.AbsenceTypeId;
            absence_task.Description = iAbsenceTaskUIDataSource.Description;
            absence_task.EndDate = iAbsenceTaskUIDataSource.EndDate;
            absence_task.StartDate = iAbsenceTaskUIDataSource.StartDate;
            absence_task.UserProfileId = iAbsenceTaskUIDataSource.UserProfileId;
            absence_task.Status = AbsenceTask.eStatusAbsence.Request;

            Boolean new_absence = false;
            if( absence_task.Id == 0 )
            {
                this._DBContext.AbsenceTasks.Add( absence_task );
                new_absence = true;
            }

            this._DBContext.SaveChanges();

            if( absence_task.Status == AbsenceTask.eStatusAbsence.Request )
            {
                if( this.OnSend != null )
                {
                    UserProfile parent_user_profile = GetParentUserProfile( absence_task.UserProfileId );
                    if( parent_user_profile != null )
                    {
                        String body_text;
                        if( new_absence )
                        {
                            body_text = String.Format( "Nueva ausencia:'{0}' solicitada por:{1}", absence_task.Description, parent_user_profile.UIName() );
                        }
                        else
                        {
                            body_text = String.Format( "Cambio de ausencia:'{0}' por el usuario:{1}", absence_task.Description, parent_user_profile.UIName() );
                        }

                        this.OnSend( this, new InformatorArgs( parent_user_profile.EMail, body_text, false, "Ausencia" ) );
                    }
                }
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileId"></param>
        /// <returns></returns>
        private UserProfile GetParentUserProfile( Int32 userProfileId )
        {
            UserProfile user_profile = this._DBContext.UserProfiles.Where( u => u.Id == userProfileId ).FirstOrDefault();
            
            if( user_profile != null )
            {
                return this._DBContext.UserProfiles.Where( u => u.Id == user_profile.ParentUserProfileId ).FirstOrDefault();
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfile"></param>
        /// <returns></returns>
        public void SaveUser( IUserProfileUIDataSource iUserProfileSource, Int32 parentUserProfileId )
        {            

            UserProfile user_profile = null;

            if( iUserProfileSource.Id == 0 )
            {
                user_profile = new UserProfile()
                {
                    Id = iUserProfileSource.Id,
                    ParentUserProfileId = parentUserProfileId
                };
            }
            else
            {
                user_profile = this._DBContext.UserProfiles.Where( up => up.Id == iUserProfileSource.Id ).FirstOrDefault();
                if( user_profile == null )
                {
                    //TODO: info
                    return;
                }
            }

            user_profile.Name = iUserProfileSource.UserName;        // 1
            user_profile.SurName = iUserProfileSource.Surname;      // 2
            user_profile.Address = iUserProfileSource.Address;      // 3
            user_profile.PhoneWork = iUserProfileSource.PhoneWork;  // 4
            user_profile.Phone = iUserProfileSource.Phone;          // 5
            user_profile.BirthDay = iUserProfileSource.BirthDay;    // 6
            user_profile.DocumentNum = iUserProfileSource.DocumentNum; // 7
            user_profile.MedicalNum = iUserProfileSource.MedicalNum;   // 8

            user_profile.EMailPrivate = iUserProfileSource.EMailPrivate;   // 9
            user_profile.EMail = iUserProfileSource.EMail;          // 10

            user_profile.ContractDate = iUserProfileSource.ContractDate; // 11
            
            if( iUserProfileSource.Status.HasValue )
            {
                user_profile.Status = iUserProfileSource.Status.Value;
            }


            if( user_profile.Id == 0 )
            {
                this._DBContext.UserProfiles.Add( user_profile );

                //TODO: Add default folders - GUANO!!!!!
                List<String> default_folder = new List<String>() { "Nominas", "PRL", "Contratos" };
                foreach( var folder_name in default_folder )
                {
                    UserFileFolder uff = new UserFileFolder() { Name = folder_name };
                    this._DBContext.UserFileFolders.Add( uff );
                }
            }

            this._DBContext.SaveChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public Boolean ChangePassword( Int32 userId, String oldPassword, String newPassword )
        {
            UserProfile user_profile = this._DBContext.UserProfiles.Where( up => up.Id == userId ).FirstOrDefault();
            if( user_profile != null )
            {
                if( String.Compare( user_profile.Password, oldPassword, false ) == 0 )
                {
                    user_profile.Password = newPassword;
                    this._DBContext.SaveChanges();

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public Boolean AdminChangePassword( Int32 userProfileId, String password )
        {
            UserProfile user_profile = this._DBContext.UserProfiles.Where( up => up.Id == userProfileId ).FirstOrDefault();

            if( user_profile != null )
            {
                user_profile.Password = password;

                this._DBContext.SaveChanges();
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileID"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ListItem> GetProjectsByUserID( Int32 userProfileID )
        {
            UserProfile user_profile = this._DBContext.UserProfiles.Include( "Projects" ).Where( up => up.Id == userProfileID ).FirstOrDefault();

            if( user_profile != null )
            {
                return user_profile.Projects.Select( p => new ListItem( p.Name, p.Id.ToString() ) ).ToList();
            }

            return new List<ListItem>();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ITask> GetProjectTaskByUserIDAndDate( Int32 userProfileID, DateTime date )
        {
            date = date.Date;
            var project_tasks = this._DBContext.ProjectTasks.Where( pt => pt.UserProfileId == userProfileID && pt.Date == date ).ToList();
            return project_tasks.Select( item => (ITask)item ).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileID"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ITask> GetAbsenceTasksProjectTaskByUserIDAndDate( Int32 userProfileID, DateTime date )
        {
            date = date.Date;
            var absence_tasks = this._DBContext.AbsenceTasks.Where( at => ( at.UserProfileId == userProfileID &&
                                                                            ( ( at.EndDate.HasValue == false && at.StartDate == date ) ||
                                                                              ( at.EndDate.HasValue && at.StartDate <= date && at.EndDate >= date ) )
                                                                          ) ).ToList();
            return absence_tasks.Select( item => (ITask) item ).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iProjectTaskUIDataSource"></param>
        public void SaveProjectTask( IProjectTaskUIDataSource iProjectTaskUIDataSource )
        {
            if( iProjectTaskUIDataSource.UserProfileId == 0 )
            {
                return; //TODO: Message
            }

            if( iProjectTaskUIDataSource.ProjectTaskID == 0 )
            {
                ProjectTask pt = new ProjectTask() {
                    UserProfileId = iProjectTaskUIDataSource.UserProfileId,
                    Date = iProjectTaskUIDataSource.Date,
                    Hours = iProjectTaskUIDataSource.Hours,
                    Description = iProjectTaskUIDataSource.Description,
                    ProjectId = iProjectTaskUIDataSource.ProjectId                    
                };
            
                this._DBContext.ProjectTasks.Add( pt );
            }
            else
            {
                ProjectTask pt = this._DBContext.ProjectTasks.Where( item => item.Id == iProjectTaskUIDataSource.ProjectTaskID ).FirstOrDefault();
                if( pt != null )
                {
                    pt.Date = iProjectTaskUIDataSource.Date;
                    pt.Description = iProjectTaskUIDataSource.Description;
                    pt.Hours = iProjectTaskUIDataSource.Hours;
                    pt.ProjectId = iProjectTaskUIDataSource.ProjectId;
                }
            }

            this._DBContext.SaveChanges();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectTaskId"></param>
        /// <returns></returns>
        public ProjectTask GetProjectTaskById( Int32 projectTaskId )
        {
           return this._DBContext.ProjectTasks.Where( pt => pt.Id == projectTaskId ).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tasks"></param>
        public void DeleteTasks( IReadOnlyCollection<TaskID> tasks )
        {
            using( var dbContextTransaction = this._DBContext.Database.BeginTransaction() )
            {
                try
                {
                    Boolean save = false;

                    foreach( TaskID task in tasks )
                    {
                        switch( task.TaskType )
                        {
                            case TaskID.eTaskEnum.eProjectTask:
                                {
                                    ProjectTask pt = this._DBContext.ProjectTasks.Where( item => item.Id == task.ID ).FirstOrDefault();
                                    if( pt != null )
                                    {
                                        save = true;
                                        this._DBContext.ProjectTasks.Remove( pt );
                                    }
                                    break;
                                }

                            case TaskID.eTaskEnum.eAusence:
                                {
                                    AbsenceTask at = this._DBContext.AbsenceTasks.Where( item => item.Id == task.ID ).FirstOrDefault();
                                    if( at != null )
                                    {
                                        save = true;
                                        this._DBContext.AbsenceTasks.Remove( at );
                                    }
                                    break;
                                }
                            default:
                                //TODO: Show WARNING!
                                break;
                        }
                    }

                    if( save )
                    {
                        this._DBContext.SaveChanges();
                    }

                    dbContextTransaction.Commit();
                }
                catch( Exception ex )
                {
                    //TODO: Show exception
                    Debug.WriteLine( String.Format( "Error:{0}", ex.Message ) );

                    dbContextTransaction.Rollback();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public IReadOnlyCollection<AbsenceTask> GetNewAbsences( DateTime date )
        {
            var t = this._DBContext.AbsenceTasks.Include("UserProfile").Include("AbsenceType").
                                                 Where( at => at.StartDate >= date && at.Status == eStatusAbsence.Request ).
                                                 ToList();

            return t;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="newStatus"></param>
        public void AbsencesChangeStatus( IReadOnlyCollection<Int32> ids, eStatusAbsence newStatus )
        {
            if( ids.Count == 0 )
            {
                return;
            }

            using( var trans = this._DBContext.Database.BeginTransaction() )
            {
                try
                {
                    List<InformatorArgs> events = new List<InformatorArgs>();

                    Boolean save = false;
                    foreach( Int32 id in ids )
                    {
                        AbsenceTask at = this._DBContext.AbsenceTasks.Include("UserProfile").Where( item => item.Id == id ).FirstOrDefault();
                        if( at != null )
                        {
                            at.Status = newStatus;
                            save = true;

                            String body;

                            if( newStatus == AbsenceTask.eStatusAbsence.Approved )
                            {
                                body = "Le comunicamos que la ausencia solicitada ha sido aprobada";
                            }
                            else
                            {
                                body = "Le comunicamos que la ausencia solicitada ha sido rechazada";
                            }


                            if( at.UserProfile != null && String.IsNullOrEmpty( at.UserProfile.EMail ) == false )
                            {
                                InformatorArgs event_item = new InformatorArgs( at.UserProfile.EMail,
                                                                                body,
                                                                                false,
                                                                                "Evaluación ausencia solicitada" );
                                events.Add( event_item );
                            }
                        }

                    }

                    if( save )
                    {
                        this._DBContext.SaveChanges();
                    }

                    trans.Commit();

                    try
                    {
                        if( this.OnSend != null )
                        {
                            foreach( var event_item in events )
                            {
                                this.OnSend( this, event_item );
                            }
                        }
                    }
                    catch( Exception ex )
                    {
                        //TODO: info
                        Debug.WriteLine( String.Format( "Error:{0}", ex.Message ) );
                    }

                }
                catch( Exception ex )
                {
                    trans.Rollback();
                    Debug.WriteLine( String.Format( "Error:{0}", ex.Message ) );
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<ListItem> GetAbsenceTypes()
        {
            var absence_types = this._DBContext.AbsenceTypes.ToList();
            
            return absence_types.Select( at => new ListItem( at.Name,
                                                             String.Format( "{0},{1}", at.id, at.Duration )
                                                             ) ).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="absenceId"></param>
        /// <returns></returns>
        public AbsenceTask GetAbsenceTaskById( Int32 absenceId )
        {
            return this._DBContext.AbsenceTasks.Where( at => at.Id == absenceId ).FirstOrDefault();
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userFileFolderId"></param>
        /// <returns></returns>
        private UserProfile GetFolderOwner( Int32 userFileFolderId )
        {
            var folder = this._DBContext.UserFileFolders.Include( "UserProfile" ).Where( f => f.Id == userFileFolderId ).FirstOrDefault();

            if( folder != null && folder.UserProfile != null )
            {
                return folder.UserProfile;
            }

            return null;
        }
    }
}
