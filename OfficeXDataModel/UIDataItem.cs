﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeXDataModel
{
    public class UIDataItem<T>
    {
        
        public T Value
        {
            get;
            private set;            
        }

        public Boolean HasValue
        {
            get;
            private set;
        }

        public UIDataItem( T value, Boolean hasValue )
        {
            this.Value = value;
            this.HasValue = hasValue;
        }
    }
}
