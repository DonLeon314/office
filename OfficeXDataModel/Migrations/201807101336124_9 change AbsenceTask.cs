namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _9changeAbsenceTask : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AbsenceTasks", "Parte");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AbsenceTasks", "Parte", c => c.Int(nullable: false));
        }
    }
}
