namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _8AbsenceandAbsenceType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AbsenceTasks", "AbsenceTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.AbsenceTasks", "AbsenceTypeId");
            AddForeignKey("dbo.AbsenceTasks", "AbsenceTypeId", "dbo.AbsenceTypes", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AbsenceTasks", "AbsenceTypeId", "dbo.AbsenceTypes");
            DropIndex("dbo.AbsenceTasks", new[] { "AbsenceTypeId" });
            DropColumn("dbo.AbsenceTasks", "AbsenceTypeId");
        }
    }
}
