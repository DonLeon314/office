namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1_First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SurName = c.String(),
                        EMail = c.String(),
                        Phone = c.String(),
                        ShowPhone = c.Boolean(nullable: false),
                        ContactName = c.String(),
                        ContactPhone = c.String(),
                        ShowContact = c.Boolean(nullable: false),
                        DocumentNum = c.String(),
                        MedicalNum = c.String(),
                        ShowDocuments = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserProfiles");
        }
    }
}
