namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12ChangeProjectproperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "ProjectContactName", c => c.String(maxLength: 50));
            AddColumn("dbo.Projects", "ProjectContactEMail", c => c.String(maxLength: 30));
            DropColumn("dbo.Projects", "Contact");
            DropColumn("dbo.Projects", "ContactEmail");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Projects", "ContactEmail", c => c.String(maxLength: 30));
            AddColumn("dbo.Projects", "Contact", c => c.String(maxLength: 50));
            DropColumn("dbo.Projects", "ProjectContactEMail");
            DropColumn("dbo.Projects", "ProjectContactName");
        }
    }
}
