namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _4editProjecttask : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectTasks", "date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProjectTasks", "date");
        }
    }
}
