namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _5editProjecttask : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProjectTasks", "Date", c => c.DateTime(nullable: false, storeType: "date"));
            AlterColumn("dbo.ProjectTasks", "Hours", c => c.Time(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProjectTasks", "Hours", c => c.Int(nullable: false));
            AlterColumn("dbo.ProjectTasks", "Date", c => c.DateTime(nullable: false));
        }
    }
}
