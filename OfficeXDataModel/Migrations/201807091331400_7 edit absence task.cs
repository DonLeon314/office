namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _7editabsencetask : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.UserProfileProjects", newName: "ProjectUserProfiles");
            DropPrimaryKey("dbo.ProjectUserProfiles");
            AddColumn("dbo.AbsenceTasks", "UserProfileId", c => c.Int(nullable: false));
            AddColumn("dbo.AbsenceTasks", "StartDate", c => c.DateTime(nullable: false, storeType: "date"));
            AddColumn("dbo.AbsenceTasks", "EndDate", c => c.DateTime(storeType: "date"));
            AddColumn("dbo.AbsenceTasks", "Parte", c => c.Int(nullable: false));
            AddColumn("dbo.AbsenceTasks", "Description", c => c.String());
            AddColumn("dbo.AbsenceTasks", "Status", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.ProjectUserProfiles", new[] { "Project_Id", "UserProfile_Id" });
            CreateIndex("dbo.AbsenceTasks", "UserProfileId");
            AddForeignKey("dbo.AbsenceTasks", "UserProfileId", "dbo.UserProfiles", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AbsenceTasks", "UserProfileId", "dbo.UserProfiles");
            DropIndex("dbo.AbsenceTasks", new[] { "UserProfileId" });
            DropPrimaryKey("dbo.ProjectUserProfiles");
            DropColumn("dbo.AbsenceTasks", "Status");
            DropColumn("dbo.AbsenceTasks", "Description");
            DropColumn("dbo.AbsenceTasks", "Parte");
            DropColumn("dbo.AbsenceTasks", "EndDate");
            DropColumn("dbo.AbsenceTasks", "StartDate");
            DropColumn("dbo.AbsenceTasks", "UserProfileId");
            AddPrimaryKey("dbo.ProjectUserProfiles", new[] { "UserProfile_Id", "Project_Id" });
            RenameTable(name: "dbo.ProjectUserProfiles", newName: "UserProfileProjects");
        }
    }
}
