namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfiles", "Status", c => c.Int(nullable: false));
            DropColumn("dbo.UserProfiles", "IsAdmin");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfiles", "IsAdmin", c => c.Boolean(nullable: false));
            DropColumn("dbo.UserProfiles", "Status");
        }
    }
}
