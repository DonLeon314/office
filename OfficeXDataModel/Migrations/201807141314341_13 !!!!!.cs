namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _13 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UserProfiles", "ShowPhone");
            DropColumn("dbo.UserProfiles", "ShowContact");
            DropColumn("dbo.UserProfiles", "ShowDocuments");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfiles", "ShowDocuments", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserProfiles", "ShowContact", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserProfiles", "ShowPhone", c => c.Boolean(nullable: false));
        }
    }
}
