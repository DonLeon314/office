namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _6addabsencetask : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AbsenceTasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AbsenceTasks");
        }
    }
}
