namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _10AddUserFileFolderandUserFile : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserFileFolders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UserProfileId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.UserProfileId, cascadeDelete: true)
                .Index(t => t.UserProfileId);
            
            CreateTable(
                "dbo.UserFiles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UserFileFolderId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserFileFolders", t => t.UserFileFolderId, cascadeDelete: true)
                .Index(t => t.UserFileFolderId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserFileFolders", "UserProfileId", "dbo.UserProfiles");
            DropForeignKey("dbo.UserFiles", "UserFileFolderId", "dbo.UserFileFolders");
            DropIndex("dbo.UserFiles", new[] { "UserFileFolderId" });
            DropIndex("dbo.UserFileFolders", new[] { "UserProfileId" });
            DropTable("dbo.UserFiles");
            DropTable("dbo.UserFileFolders");
        }
    }
}
