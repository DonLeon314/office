namespace OfficeXDataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _15Changeuserprofile : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserProfiles", "Address", c => c.String());
            AddColumn("dbo.UserProfiles", "EMailPrivate", c => c.String());
            AddColumn("dbo.UserProfiles", "BirthDay", c => c.DateTime(nullable: false));
            AddColumn("dbo.UserProfiles", "PhoneWork", c => c.String());
            AddColumn("dbo.UserProfiles", "ContractDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.UserProfiles", "ContactName");
            DropColumn("dbo.UserProfiles", "ContactPhone");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProfiles", "ContactPhone", c => c.String());
            AddColumn("dbo.UserProfiles", "ContactName", c => c.String());
            DropColumn("dbo.UserProfiles", "ContractDate");
            DropColumn("dbo.UserProfiles", "PhoneWork");
            DropColumn("dbo.UserProfiles", "BirthDay");
            DropColumn("dbo.UserProfiles", "EMailPrivate");
            DropColumn("dbo.UserProfiles", "Address");
        }
    }
}
