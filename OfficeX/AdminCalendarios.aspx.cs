﻿using OfficeXDataModel;
using OfficeXDataModel.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX
{
    public partial class AdminCalendarios: System.Web.UI.Page
    {
        private OfficeXContext _OfficeXContext = new OfficeXContext();

        protected void Page_Load( object sender, EventArgs e )
        {
            this.CheckAutirize();

            if( this.IsPostBack == false )
            {
                RefreshUsers();
            }
        }

        private void RefreshUsers()
        {
            Int32 current_user_profile_id = Page.GetCurrentUserID();
            if( current_user_profile_id == 0 )
            {
                return;
            }

            var users = this._OfficeXContext.GetChildUsers( current_user_profile_id );

            this.ddlUsers.Items.Clear();

            users.ForEach( item => this.ddlUsers.Items.Add( item ) );
        }

        private Int32 GetSelectedUserId()
        {
            ListItem selected_user = this.ddlUsers.SelectedItem;
            if( selected_user == null )
            {
                return 0;
            }

            Int32 result = 0;

            Int32.TryParse( selected_user.Value, out result );

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Calendar_DayRender( object sender, DayRenderEventArgs e )
        {
            Calendar ctrl = (Calendar) sender;
            if( ctrl == null )
            {
                return;
            }

            Int32 user_id = GetSelectedUserId();

            TaskBuilder tb = new TaskBuilder();

            var items = tb.Build( this._OfficeXContext, user_id, e.Day.Date );

            
            if( items.Count > 0 )
            {
                    ListBox list_box = new ListBox();

                    list_box.CssClass = "day_list";

                    list_box.Items.AddRange( items.ToArray() );

                    e.Cell.Controls.Add( new LiteralControl( "<br />" ) );
                    e.Cell.Controls.Add( list_box );            
            }
        }

        protected void ddlUsers_SelectedIndexChanged( object sender, EventArgs e )
        {

        }
    }
}