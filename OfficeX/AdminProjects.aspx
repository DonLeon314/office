﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminProjects.aspx.cs"
    Inherits="OfficeX.AdminProjects" meta:resourcekey="PageResource1" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%@ Register TagPrefix="uc" TagName="ProjectEditor" Src="~/UserControls/ucProjectEditor.ascx" %>

    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        td {
            cursor: pointer;
        }

        .hover_row {
            background-color: #FFFFBF;
        }
    </style>
   
    <br />
    <table style="width: 100%">
        <tr>
            <td width="50%">
                <div style="width: 100%">
                    <asp:GridView ID="gvProjects" class="gridProjects" runat="server" AutoGenerateColumns="False" Width="100%"
                        DataKeyNames="Id" AllowPaging="True" PageSize="10"
                        OnPageIndexChanging="gvProjects_PageIndexChanging" OnRowCommand="gvProjects_RowCommand"
                        OnRowDataBound="gvX_RowDataBound" meta:resourcekey="gvProjectsResource1"
                        OnSelectedIndexChanged="gvProjects_SelectedIndexChanged">
                        <Columns>
                            <asp:BoundField DataField="Name" meta:resourcekey="BoundFieldName" />
                        </Columns>
                        <SelectedRowStyle ForeColor="White" Font-Bold="True" BackColor="#CE5D5A"></SelectedRowStyle>
                    </asp:GridView>
                    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
                    <script type="text/javascript">
                        $(function () {
                            $("[id*=gvProjects] td").hover(function () {
                                $("td", $(this).closest("tr")).addClass("hover_row");
                            },
                                function () {
                                    $("td", $(this).closest("tr")).removeClass("hover_row");
                                });
                        });
                    </script>
                    <asp:Button ID="btnAddProject" runat="server" class="btn btn-default" meta:resourcekey="btnAddProject" OnClick="btnAddProject_Click" />
                    <asp:Button ID="btnDeleteProject" runat="server" class="btn btn-default" meta:resourcekey="btnDeleteProject" OnClick="btnDeleteProject_Click" />
                </div>
            </td>
            <td>
                <div style="height: 100%">
                    <uc:ProjectEditor ID="ucProjectEditor" runat="server" />
                </div>
            </td>
        </tr>
    </table>

    <br />
    <br />
</asp:Content>
