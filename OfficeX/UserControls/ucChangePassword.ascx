﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucChangePassword.ascx.cs" Inherits="OfficeX.UserControls.ucChangePassword" %>
<div>
    <asp:Label runat="server" meta:resourcekey="lbChangePassword" ></asp:Label><br />
    <asp:Label runat="server" meta:resourcekey="lbPassword" ></asp:Label><asp:TextBox ID="tbPassword" runat="server" ></asp:TextBox><br />
    <asp:Label runat="server" meta:resourcekey="lbConfirmPassword" ></asp:Label><asp:TextBox ID="tbConfirmPassword" runat="server" ></asp:TextBox><br />
    <br />
    <asp:Label Visible="false" ForeColor="Red" ID="lblCheck" runat="server">Las contraseñas no coinciden</asp:Label>
    <br />
    <asp:Button ID="btnSave" runat="server" style="padding:10px" class="btn btn-default" meta:resourcekey="btnSave" OnClick="btnSave_Click" />
    <asp:Button ID="btnCancel" runat="server" style="padding:10px" class="btn btn-default" meta:resourcekey="btnCancel" OnClick="btnCancel_Click" />
    <asp:TextBox ID="tbID" runat="server" Style="display: none;" />
</div>