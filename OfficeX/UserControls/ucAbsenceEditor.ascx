﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAbsenceEditor.ascx.cs" Inherits="OfficeX.UserControls.ucAbsenceEditor" %>

<script type="text/javascript" src="Scripts/jquery-3.3.1.js"></script>
<script type="text/javascript" src="Scripts/jQuery-UI/jquery-ui.js"></script>
<script type="text/javascript" src="Scripts/jQuery-UI/i18n/datepicker-es.js"></script>
<script type="text/javascript" src="Scripts/jQuery-UI/i18n/datepicker-ru.js"></script>
<script type="text/javascript" src="Scripts/Office/ucAbsenceEditor.js"></script>

<div style="padding:10px; margin:10px 10px 10px 10px; border-radius:0 10px;border:1px solid #7a7b7e; height:100%">
    <asp:DropDownList ID="ddlAbsenceTypes" ClientID="ddlAbsenceTypes" runat="server" Width="100%" onchange="OnChangeDuration()"> </asp:DropDownList>
    <asp:TextBox ID="tbStartDatePicker" ClientID="tbStartDatePicker" runat="server" ></asp:TextBox>
    <asp:Panel ID="panelEndDatePicker" ClientID="panelEndDatePicker" runat="server">
    <asp:TextBox ID="tbEndDatePicker" ClientID="tbEndDatePicker" runat="server" ></asp:TextBox>
    </asp:Panel>
    <asp:Label Text="Description" runat="server" Width="100%" meta:resourcekey="labelDescription"/>
    <asp:TextBox ID="tbDescription" runat="server" Width="100%"/>
    <br />
    <br />
    <asp:Button ID="btnSave" runat="server" style="padding:10px" class="btn btn-default" meta:resourcekey="btnSave" OnClick="btnSave_Click" />
    <asp:Button ID="btnCancel" runat="server" style="padding:10px" class="btn btn-default" meta:resourcekey="btnCancel" OnClick="btnCancel_Click" />

    <asp:TextBox ID="tbDateCulture" ClientID="tbDateCulture" runat="server" Style="display: none;" />
    <asp:TextBox ID="tbUserID" runat="server" Style="display: none;" />
    <asp:TextBox ID="tbAbsenceTaskID" runat="server" Style="display: none;" />
</div>