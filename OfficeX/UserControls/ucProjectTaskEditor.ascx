﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProjectTaskEditor.ascx.cs" Inherits="OfficeX.UserControls.ucProjectTaskEditor" %>
<div style="padding:10px; margin:10px 10px 10px 10px; border-radius:0 10px;border:1px solid #7a7b7e; height:100%">
    
    <asp:Label Text="New Task" runat="server" meta:resourcekey="LabelResource1"></asp:Label><br />
    <asp:Label Text="Projects:" runat="server" meta:resourcekey="LabelResource2"></asp:Label><asp:DropDownList ID="ddlProjects" runat="server" meta:resourcekey="ddlProjectsResource1"></asp:DropDownList>
    <asp:Calendar ID="Calendar" runat="server" Caption="weeee" meta:resourcekey="CalendarResource1"></asp:Calendar>
    <asp:Label Text="Hours:" runat="server" meta:resourcekey="LabelResource4"></asp:Label><asp:TextBox ID="tbHoras" runat="server" meta:resourcekey="tbHorasResource1"></asp:TextBox>

    <asp:Label Text="Description:" runat="server" meta:resourcekey="LabelResource5"></asp:Label><asp:TextBox ID="tbDescription" runat="server" meta:resourcekey="tbDescriptionResource1"></asp:TextBox>

    <asp:Button ID="btnSave" runat="server" style="padding:10px" class="btn btn-default" meta:resourcekey="btnSaveResource1" OnClick="btnSave_Click"  />
    <asp:Button ID="btnCancel" runat="server" style="padding:10px" class="btn btn-default" meta:resourcekey="btnCancelResource1" OnClick="btnCancel_Click" />

    <asp:TextBox ID="tbUserID" runat="server" Style="display: none;" meta:resourcekey="tbUserIDResource1" />
    <asp:TextBox ID="tbProjectTaskID" runat="server" Style="display: none;" meta:resourcekey="tbProjectTaskIDResource1" />
</div>

