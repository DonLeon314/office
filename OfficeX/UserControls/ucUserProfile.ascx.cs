﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeXDataModel;
using OfficeXDataModel.Interfaces;
using OfficeXDataModel.Model;

namespace OfficeX.UserControls
{
    public partial class ucUserProfile: 
                         System.Web.UI.UserControl,
                         IUserProfileUIDataSource
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load( object sender, EventArgs e )
        {

        }

        #region interface IUserProfileUIDataSource
        /// <summary>
        /// 
        /// </summary>
        public Int32 Id
        {
            get
            {
                Int32 id = 0;

                return ( Int32.TryParse( this.tbID.Text, out id ) ? id : 0 );
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public String UserName // 1
        {
            get
            {
                return this.tbUserName.Text.Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Surname  // 2
        {
            get
            {
                return this.tbSurname.Text.Trim();
            }
        }


        public String Address // 3
        {
            get
            {
                return this.tbAddress.Text.Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Phone // 4
        {
            get
            {
                return this.tbPhone.Text.Trim();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public String PhoneWork // 5
        {
            get
            {
                return this.tbWorkPhone.Text.Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime BirthDay // 6
        {
            get
            {
                DateTime result;
                if( DateTime.TryParse( this.tbBirthDay.Text.Trim(), out result ) == false )
                {
                    result = DateTime.Now;
                }
                else
                {
                    result = result.Date;
                }
                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String DocumentNum // 7
        {
            get
            {
                return this.tbDocumentNum.Text.Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String MedicalNum  // 8
        {
            get
            {
                return this.tbMedicalNum.Text.Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String EMailPrivate // 9
        {
            get
            {
                return this.tbEMailPersonal.Text.Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String EMail // 10
        {
            get
            {
                return this.tbEMail.Text.Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime ContractDate // 11
        {
            get
            {
                DateTime result;
                if( false == DateTime.TryParse( this.tbContractDate.Text.Trim(), out result ) )
                {
                    result = DateTime.Now.Date;
                }
                else
                {
                    result = result.Date;
                }
                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public UserProfile.eStatus? Status
        {
            get
            {
                if( String.Compare( this.panelStatus.Style["display"], "none", 0 ) == 0 )
                {
                    return null;
                }

                Int32 value = (Int32) UserProfile.eStatus.User;
                ListItem selected_status = this.rbStatus.SelectedItem;
                if( selected_status != null )
                {
                    Int32.TryParse( selected_status.Value, out value );
                }

                UserProfile.eStatus result = (UserProfile.eStatus)( value );

                return result;
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfile"></param>
        /// <param name="showSatus"></param>
        public void ToControls( IUserProfileContextDataSource userProfile, Boolean showSatus )
        {
            if( userProfile != null )
            {

                this.tbID.Text = userProfile.Id.ToString();

                this.tbUserName.Text = userProfile.Name;        // 1
                this.tbSurname.Text = userProfile.SurName;      // 2
                this.tbAddress.Text = userProfile.Address;      // 3
                this.tbPhone.Text = userProfile.Phone;          // 4
                this.tbWorkPhone.Text = userProfile.PhoneWork;  // 5
                this.tbBirthDay.Text = userProfile.BirthDay.ToShortDateString(); // 6
                this.tbDocumentNum.Text = userProfile.DocumentNum;  // 7
                this.tbMedicalNum.Text = userProfile.MedicalNum;    // 8
                this.tbEMailPersonal.Text = userProfile.EMailPrivate;  // 9
                this.tbEMail.Text = userProfile.EMail;                // 10
                this.tbContractDate.Text = userProfile.ContractDate.ToShortDateString(); // 11

                this.rbStatus.SelectedValue = ( (Int32) userProfile.Status ).ToString();
            }
            else
            {
                this.tbID.Text = "0";

                this.tbUserName.Text = String.Empty;        // 1
                this.tbSurname.Text = String.Empty;      // 2
                this.tbAddress.Text = String.Empty;      // 3
                this.tbPhone.Text = String.Empty;          // 4
                this.tbWorkPhone.Text = String.Empty;  // 5
                this.tbBirthDay.Text = DateTime.Now.Date.ToShortDateString(); // 6
                this.tbDocumentNum.Text = String.Empty;  // 7
                this.tbMedicalNum.Text = String.Empty;    // 8
                this.tbEMailPersonal.Text = String.Empty;  // 9
                this.tbEMail.Text = String.Empty;                // 10
                this.tbContractDate.Text = DateTime.Now.Date.ToShortDateString(); // 11

                this.rbStatus.SelectedValue = ( (Int32) UserProfile.eStatus.User ).ToString();
            }

            if( showSatus )
            {
                this.panelStatus.Style["display"] = "inline";
            }
            else
            {
                this.panelStatus.Style["display"] = "none";
            }            
        }
    }
}