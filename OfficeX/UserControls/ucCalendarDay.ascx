﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCalendarDay.ascx.cs" Inherits="OfficeX.UserControls.ucCalendarDay" %>

<script type="text/javascript" src="Scripts/jquery-3.3.1.js"></script>

<script type="text/javascript">

    function OnAddProjectTask() {
        __doPostBack('AddProjectTask', '');

        return false;
    }

    function OnAddAbsenceTask() {
        __doPostBack('AddAbsenceTask', '');

        return false;
    }

    function OnDeleteTasks() {

        var ids = GetSelectedTask();

        if (ids != undefined && ids.length != 0) {

            var locResponse = confirm("¿Confirma el borrado?");
            if (locResponse == true) {
                __doPostBack('DeleteTasks', ids);
            }
        };

        return false;
    }

    function OnEditTask() {
        var ids = GetSelectedTask();

        if (ids != undefined && ids.length != 0) {

            __doPostBack('EditTask', ids);
        };

        return false;
    }

    function GetSelectedTask() {
        var ids = '';

        var dpt = $('#<%= lbDay.ClientID %>');

        var options = dpt.children('option:selected');
        $.each(options, function (i, el) {
            ids += $(el).val();
            if (i < options.length - 1) {
                ids += ',';
            };
        });

        return ids;
    }

</script>


<asp:Panel ID="PanelDay" runat="server">
    <asp:ListBox ID="lbDay" runat="server" SelectionMode="Multiple" Width="100%" BackColor="#CCCCCC" ForeColor="Black"></asp:ListBox><br />
    <asp:Button ID="btnAddProjectTask" runat="server" Style="padding: 10px" class="btn btn-default" meta:resourcekey="btnAddProjectTask" OnClientClick="return OnAddProjectTask();" />
    <asp:Button ID="btnAddAbsenceTask" runat="server" Style="padding: 10px" class="btn btn-default" meta:resourcekey="btnAddAbsenceTask" OnClientClick="return OnAddAbsenceTask();" />
    <asp:Button ID="btnEditTask" runat="server" Style="padding: 10px" class="btn btn-default" meta:resourcekey="btnEditTask" OnClientClick="return OnEditTask();" />
    <asp:Button ID="btnDeleteTasks" runat="server" Style="padding: 10px" class="btn btn-default" meta:resourcekey="btnDeleteTask" OnClientClick="return OnDeleteTasks();" />
</asp:Panel>
