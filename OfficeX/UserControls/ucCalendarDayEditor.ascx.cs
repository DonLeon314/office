﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX.UserControls
{
    public partial class ucCalendarDayEditor: System.Web.UI.UserControl
    {
        public EventHandler OnHide;

        protected void Page_Load( object sender, EventArgs e )
        {
            //this.Border.Style["width"]="20%";
        }

        protected void btnHide_Click( object sender, EventArgs e )
        {
            if( this.OnHide != null )
            {
                this.OnHide( sender, e );
            }
        }

        public Unit Width
        {
            get
            {
                return new Unit( this.Border.Style["width"] );
            }
            set
            {
                this.Border.Style["width"] = value.ToString();
            }
        }
    }
}