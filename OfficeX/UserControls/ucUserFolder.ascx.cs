﻿using OfficeX.Utils;
using OfficeXDataModel.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX.UserControls
{
    public partial class ucUserFolder: System.Web.UI.UserControl
    {
        protected void Page_Load( object sender, EventArgs e )
        {

        }

        public void ToControls( String title, String userFolderId, IReadOnlyCollection<ListItem> folderFiles )
        {
            this.Label1.Text = title;

            this.lbFiles.Items.Clear();

            this.tbFolderId.Text = userFolderId;

           folderFiles.ForEach( item => this.lbFiles.Items.Add( item ) );
        }

        protected void lbFiles_SelectedIndexChanged( object sender, EventArgs e )
        {
            Int32 id_user = Page.GetCurrentUserID();
            if( id_user == 0 )
            {
                return;
            }

            ListItem selected = this.lbFiles.SelectedItem;
            if( selected == null )
            {
                return;
            }

            FolderIDInfo folder_id_info = FolderIDInfo.FromString( selected.Value );
            if( folder_id_info == null )
            {
                return;
            }

            String url = String.Empty;
            switch( folder_id_info.FolderType )
            {
                case FolderIDInfo.eFolderType.Private:
                    {
                        url = @"Files/" + id_user.ToString() + @"/" + tbFolderId.Text + @"/" + selected.Text;
                        break;
                    }

                case FolderIDInfo.eFolderType.Shared:
                    {
                        url = @"SharedFiles/" + tbFolderId.Text + @"/" + selected.Text;
                        break;
                    }
            }

            if( String.IsNullOrWhiteSpace( url ) == false )
            {
                Page.Response.Redirect( url );
            }

        }
    }
}