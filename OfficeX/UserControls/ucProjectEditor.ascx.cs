﻿using OfficeXDataModel;
using OfficeXDataModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX.UserControls
{
    public partial class ProjectEditor:
                    System.Web.UI.UserControl,
                    IProjectUIDataSource
    {
        public event EventHandler OnOKClick;
        public event EventHandler OnCANCELClick;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load( object sender, EventArgs e )
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iProjectContextDataSource"></param>
        public void ToControls( IProjectContextDataSource iProjectContextDataSource, IEnumerable<ListItem> users )
        {
            if( iProjectContextDataSource == null )
            {
                this.tbProjectName.Text = "";
                this.tbID.Text = "0";
                this.tbProjectContactName.Text = "";
                this.tbProjectContactEMail.Text = "";
            }
            else
            {
                this.tbProjectName.Text = iProjectContextDataSource.Name;
                this.tbID.Text = iProjectContextDataSource.Id.ToString();
                this.tbProjectContactName.Text = iProjectContextDataSource.ProjectContactName;
                this.tbProjectContactEMail.Text = iProjectContextDataSource.ProjectContactEMail;
            }
            
            this.lbChildUsers.Items.Clear();

            foreach( var item in users )
            {
                this.lbChildUsers.Items.Add( item );
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click( object sender, EventArgs e )
        {
            if( this.OnCANCELClick != null )
            {
                this.OnCANCELClick( sender, e );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click( object sender, EventArgs e )
        {
            lblCheck.Visible = false;

            bool locAnyUserChecked = false;

            foreach (ListItem item in this.lbChildUsers.Items)
            {
                if (item.Selected ) locAnyUserChecked = true;
            }

            if (locAnyUserChecked)
            {
                if (this.OnOKClick != null)
                {
                    this.OnOKClick(sender, e);
                }
            }
            else
            {
                lblCheck.Visible = true;
            }

        }

        #region interface IProjectUIDataSource

        /// <summary>
        /// 
        /// </summary>
        public UIDataItem<String> ProjectName
        {
            get
            {
                return new UIDataItem<String>( this.tbProjectName.Text, true );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public UIDataItem<Int32> Id
        {
            get
            {
                Int32 id = 0;
                Boolean has_value = Int32.TryParse( this.tbID.Text, out id );
                
                return new UIDataItem<Int32>( id, has_value );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Int32> UsersChild
        {
            get
            {
                foreach( ListItem item in this.lbChildUsers.Items )
                {
                    if( item.Selected )
                    {
                        Int32 id = 0;
                        if( Int32.TryParse( item.Value, out id ) && id != 0 )
                        {
                            yield return id;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String ProjectContactName
        {
            get
            {
                return this.tbProjectContactName.Text.Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String ProjectContactEMail
        {
            get
            {
                return this.tbProjectContactEMail.Text.Trim();
            }
        }

        #endregion
    }
}