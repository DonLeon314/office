﻿using OfficeXDataModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX.UserControls
{
    public partial class ucProjectTaskEditor:
                    System.Web.UI.UserControl,
                    IProjectTaskUIDataSource
    {

        public event EventHandler OnOKClick;
        public event EventHandler OnCANCELClick;

        protected void Page_Load( object sender, EventArgs e )
        {

        }

        public void ToControls( IProjectTaskDataSource dataSource )
        {
            this.tbProjectTaskID.Text = dataSource.ProjectTaskID.ToString();
            this.tbUserID.Text = dataSource.UserProfileID.ToString();

            this.tbHoras.Text = dataSource.Hours.ToString(@"hh\:mm");            
            this.tbDescription.Text = dataSource.Description;

            this.ddlProjects.Items.Clear();            
            foreach( var item in dataSource.Projects )
            {
                this.ddlProjects.Items.Add( item );
            }

            if( dataSource.ProjectID != 0 )
            {
                this.ddlProjects.SelectedValue = dataSource.ProjectID.ToString();
            }

            this.Calendar.SelectedDate = dataSource.Date;
            this.Calendar.VisibleDate = dataSource.Date;

        }

        #region interface IProjectTaskUIDataSource
        
        public Int32 ProjectTaskID
        {
            get
            {
                Int32 id = 0;
                return ( Int32.TryParse( this.tbProjectTaskID.Text, out id ) ? id : 0 );                
            }
        }

        public Int32 UserProfileId
        {
            get
            {
                Int32 id = 0;
                return ( Int32.TryParse( this.tbUserID.Text, out id ) ? id : 0 );
            }
        }

        public Int32 ProjectId
        {
            get
            {
                Int32 id = 0;
                ListItem selected = this.ddlProjects.SelectedItem;
                if( selected != null )
                {
                    if( Int32.TryParse( selected.Value, out id ) )
                    {
                        return id;
                    }
                }

                return 0;
            }
        }

        public String Description
        {
            get
            {
                String result = this.tbDescription.Text.Trim();

                if( String.IsNullOrWhiteSpace( result ) )
                {
                    var selected = this.ddlProjects.SelectedItem;
                    if( selected != null )
                    {
                        result = selected.Text;
                    }
                }

                return result;
            }
        }

        public TimeSpan Hours
        {
            get
            {
                TimeSpan time;
                TimeSpan.TryParse( this.tbHoras.Text, out time );
                return time;
            }
        }

        public DateTime Date
        {
            get
            {
                return this.Calendar.SelectedDate.Date;
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click( object sender, EventArgs e )
        {
            if( this.OnOKClick != null )
            {
                this.OnOKClick( sender, e );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click( object sender, EventArgs e )
        {
            if( this.OnCANCELClick != null )
            {
                this.OnCANCELClick( sender, e );
            }
        }
    }
}