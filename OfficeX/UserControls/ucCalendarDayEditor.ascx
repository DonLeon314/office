﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucCalendarDayEditor.ascx.cs" Inherits="OfficeX.UserControls.ucCalendarDayEditor" %>
<div ID ="Border" runat="server" style ="padding:10px; margin:10px 10px 10px 10px; border-radius:0 10px;border:1px solid #7a7b7e; height:100%">
    <asp:GridView ID="gvDayTasks" runat="server">

    </asp:GridView><br /><br />
    <asp:Button ID="btnAddProjectTask" runat="server" style="padding:10px" class="btn btn-default" />
    <asp:Button ID="btnDeleteTask" runat="server" style="padding:10px" class="btn btn-default"  />
    <br /><br />
    <asp:Button ID="btnHide" runat="server" style="padding:10px" class="btn btn-default" OnClick="btnHide_Click" />    
</div>