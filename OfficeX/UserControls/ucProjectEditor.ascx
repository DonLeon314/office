﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProjectEditor.ascx.cs" Inherits="OfficeX.UserControls.ProjectEditor" %>
<div style="padding:10px; margin:10px 10px 10px 10px; border-radius:0 10px;border:1px solid #7a7b7e; height:100%">
    <asp:Label runat="server" Width="100%" meta:resourcekey="lbProject"></asp:Label><br />
    <asp:TextBox ID="tbProjectName" runat="server" Width="100%" required="required" ></asp:TextBox><br /><br />
    <asp:CheckBoxList  ID="lbChildUsers" runat="server"> </asp:CheckBoxList><br /><br />
    <asp:Label runat="server" Width="100%" meta:resourcekey="lbProjectContactName"></asp:Label><br />
    <asp:TextBox ID="tbProjectContactName" runat="server" Width="100%" required="required"></asp:TextBox><br /><br />
    <asp:Label runat="server" Width="100%" meta:resourcekey="lbProjectContactEMail" required="required"></asp:Label><br />
    <asp:TextBox ID="tbProjectContactEMail" runat="server" Width="100%" required="required"></asp:TextBox><br /><br />
    <asp:Label Visible="false" ForeColor="Red" ID="lblCheck" runat="server">Debe seleccionar al menos un usuario.</asp:Label>
    <br />
    <asp:Button ID="btnSave" runat="server" style="padding:10px" class="btn btn-default" meta:resourcekey="btnSave" OnClick="btnSave_Click" />
    <asp:Button ID="btnCancel" runat="server" style="padding:10px" class="btn btn-default" meta:resourcekey="btnCancel" OnClick="btnCancel_Click" />
    <asp:TextBox ID="tbID" runat="server" Style="display: none;" />
</div>