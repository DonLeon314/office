﻿using OfficeX.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using OfficeXDataModel.Utils;
using static OfficeXDataModel.Model.AbsenceTask;
using OfficeXDataModel.Interfaces;

namespace OfficeX.UserControls
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ucAbsenceEditor: 
                System.Web.UI.UserControl,
                IAbsenceTaskUIDataSource
    {
        public event EventHandler OnOKClick;
        public event EventHandler OnCANCELClick;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load( object sender, EventArgs e )
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click( object sender, EventArgs e )
        {
            if( this.OnCANCELClick != null )
            {
                this.OnCANCELClick( sender, e );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click( object sender, EventArgs e )
        {
            if( this.OnOKClick != null )
            {
                this.OnOKClick( sender, e );
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="absenceSource"></param>
        public void ToControls( AbsenceTaskSource absenceSource )
        {
            this.tbUserID.Text = absenceSource.UserProfileId.ToString();
            this.tbAbsenceTaskID.Text = absenceSource.Id.ToString();
            

            // types
            this.ddlAbsenceTypes.Items.Clear();
            absenceSource.AbsenceTypes.ForEach( item => this.ddlAbsenceTypes.Items.Add( item ) );
                        

            if( absenceSource.AbsenceTypes.Where( item => item.Value == absenceSource.AbsenceTypeId.ToString() ).FirstOrDefault() != null )
            {
                this.ddlAbsenceTypes.SelectedValue = absenceSource.AbsenceTypeId.ToString();
            }
            

            this.tbDescription.Text = absenceSource.Description;
            
            var c = Thread.CurrentThread.CurrentCulture;

            this.tbDateCulture.Text = "es"; //c.TwoLetterISOLanguageName;

            this.tbStartDatePicker.Text = absenceSource.StartDate.ToShortDateString();

            if( absenceSource.EndDate.HasValue )
            {
                this.tbEndDatePicker.Text = absenceSource.EndDate.Value.ToShortDateString();
              //  this.panelEndDatePicker.Style.Add( "display", "inline" );
            }
            else
            {
                this.tbEndDatePicker.Text = "";
             //   this.panelEndDatePicker.Style.Add( "display", "none" );
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlAbsenceTypes_SelectedIndexChanged( object sender, EventArgs e )
        {
            Debug.WriteLine( "ddlAbsenceTypes_SelectedIndexChanged" );
        }

        #region interface IAbsenceTaskUIDataSource        
        /// <summary>
        /// 
        /// </summary>
        public Int32 Id
        {
            get
            {
                Int32 id = 0;
                return ( Int32.TryParse( this.tbAbsenceTaskID.Text, out id ) ? id : 0 );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String Description
        {
            get
            {
                String result = this.tbDescription.Text.Trim();

                if( String.IsNullOrWhiteSpace( result ) )
                {
                    ListItem selected_type = this.ddlAbsenceTypes.SelectedItem;
                    if( selected_type != null )
                    {
                        result = selected_type.Text;
                    }
                }
                return result;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 UserProfileId
        {
            get
            {
                Int32 id = 0;

                return ( Int32.TryParse( this.tbUserID.Text, out id ) ? id : 0 );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime StartDate
        {
            get
            {
                DateTime value;
                return ( DateTime.TryParse( this.tbStartDatePicker.Text.Trim(), out value ) ? value : DateTime.Now );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? EndDate
        {
            get
            {                

                if( String.IsNullOrWhiteSpace( this.tbEndDatePicker.Text ) == false )
                {
                    DateTime value;
                    if( DateTime.TryParse( this.tbEndDatePicker.Text.Trim(), out value ) )
                    {
                        return value;
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public eStatusAbsence Status
        {
            get
            {
                return eStatusAbsence.Request; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 AbsenceTypeId
        {
            get
            {
                ListItem selected = this.ddlAbsenceTypes.SelectedItem;
                if( selected != null )
                {
                    String[] tokens = selected.Value.Split( new Char[] {','} );
                    if( tokens.Length > 0 )
                    {
                        Int32 id = 0;
                        if( Int32.TryParse( tokens[0], out id ) )
                        {
                            return id;
                        }
                    }
                }
                return 0;
            }
        }

        #endregion
    }
}