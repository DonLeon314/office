﻿using OfficeXDataModel.Interfaces;
using System;


namespace OfficeX.UserControls
{
    public partial class ucChangePassword: 
                    System.Web.UI.UserControl,
                    IPasswordUIDataSource
    {

        public event EventHandler OnSave;
        public event EventHandler OnHide;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load( object sender, EventArgs e )
        {

        }

        #region interface IPasswordUIDataSource        
        public Int32 Id
        {
            get
            {
                Int32 id = 0;
                Int32.TryParse( this.tbID.Text, out id );

                return id;
            }
        }

        public String Password
        {
            get
            {
                return this.tbPassword.Text;
            }
        }

        #endregion



        /// <summary>
        /// 
        /// </summary>
        public void ToControls( Int32 userProfileId )
        {
            this.tbID.Text = userProfileId.ToString();

            this.tbPassword.Text = String.Empty;
            this.tbConfirmPassword.Text = String.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click( object sender, EventArgs e )
        {

            String p1 = this.tbPassword.Text;
            String p2 = this.tbConfirmPassword.Text;

            lblCheck.Visible = false;

            if (String.IsNullOrWhiteSpace(p1) || String.IsNullOrWhiteSpace(p2))
                lblCheck.Text = "Debe rellenar los dos campos de contraseña";
            else
                lblCheck.Text = "Las contraseñas no coinciden";


            if ( String.IsNullOrWhiteSpace( p1 ) || String.IsNullOrWhiteSpace( p2 ) ||
                String.Compare( p1, p2, false ) != 0 )
            {
                lblCheck.Visible = true;
                return;
            }

            if( this.OnSave != null )
            {
                this.OnSave( this, new EventArgs() );
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click( object sender, EventArgs e )
        {
            if( this.OnHide != null )
            {
                this.OnHide( this, new EventArgs() );
            }
        }
    }
}