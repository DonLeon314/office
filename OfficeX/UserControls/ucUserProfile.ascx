﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucUserProfile.ascx.cs" Inherits="OfficeX.UserControls.ucUserProfile" %>

<script type="text/javascript" src="Scripts/jquery-3.3.1.js"></script>
<script type="text/javascript" src="Scripts/jQuery-UI/jquery-ui.js"></script>
<script type="text/javascript" src="Scripts/jQuery-UI/i18n/datepicker-es.js"></script>
<script type="text/javascript" src="Scripts/jQuery-UI/i18n/datepicker-ru.js"></script>

<script type="text/javascript" src="Scripts/Office/UserProfile.js"></script>


<script type="text/javascript">

    function allowOnlyNumber(evt)
{
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;
  return true;
}

</script>

<div>
    <asp:Label runat="server" Text="Nombre:"></asp:Label><asp:TextBox ID="tbUserName" runat="server" required="required"></asp:TextBox><br />
    <asp:Label runat="server" Text="Apellidos:"></asp:Label><asp:TextBox ID="tbSurname" runat="server"></asp:TextBox><br />
    <br />
    <asp:Label runat="server" Text="Direccion:"></asp:Label><asp:TextBox ID="tbAddress" runat="server"></asp:TextBox><br />
    <br />
    <asp:Label runat="server" Text="Telefono:"></asp:Label><asp:TextBox ID="tbPhone" runat="server" onkeypress="return allowOnlyNumber(event);"></asp:TextBox><br />
    <asp:Label runat="server" Text="Telefono de empresa:"></asp:Label><asp:TextBox ID="tbWorkPhone" runat="server" onkeypress="return allowOnlyNumber(event);"></asp:TextBox><br />
    <asp:Label runat="server" Text="Fecha de nacimiento:"></asp:Label><asp:TextBox ID="tbBirthDay" ClientID="tbBirthDay" runat="server"></asp:TextBox><br />
    <asp:Label runat="server" Text="DNI:"></asp:Label><asp:TextBox ID="tbDocumentNum" runat="server"></asp:TextBox><br />
    <asp:Label runat="server" Text="Numero de la Seguridad social:"></asp:Label><asp:TextBox ID="tbMedicalNum" runat="server"></asp:TextBox><br />
    <asp:Label runat="server" Text="EMail personal:"></asp:Label><asp:TextBox ID="tbEMailPersonal" runat="server" TextMode="Email"></asp:TextBox><br />
    <asp:Label runat="server" Text="EMail empresa:"></asp:Label><asp:TextBox ID="tbEMail" runat="server" required="required" TextMode="Email"></asp:TextBox><br />
    <asp:Label runat="server" Text="Fecha inicio contrato:"></asp:Label><asp:TextBox ID="tbContractDate" ClientID="tbContractDate" runat="server"></asp:TextBox><br />

    <asp:Panel runat="server" ID="panelStatus">
        <div style="border: 1px solid">
            <asp:Label runat="server" Text="Status:"></asp:Label>
            <asp:RadioButtonList ID="rbStatus" runat="server">
                <asp:ListItem Value="0">Admin</asp:ListItem>
                <asp:ListItem Value="1">User</asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </asp:Panel>
    <asp:TextBox ID="tbID" runat="server" Style="display: none;" />
    <asp:TextBox ID="tbDateCulture" Text="es" ClientID="tbDateCulture" runat="server" Style="display: none;" />
</div>
