﻿using OfficeX.UserControls;
using OfficeX.Utils;
using OfficeXDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX
{
    public partial class UserFiles: System.Web.UI.Page
    {

        private OfficeXContext _OfficeXContext = new OfficeXContext();

        protected void Page_Load( object sender, EventArgs e )
        {

            this.CheckAutirize();

            if( this.IsPostBack == false )
            {
                
            }

            RefreshFolder();

        }

        void RefreshFolder()
        {
            Int32 user_id = Page.GetCurrentUserID();
            if( user_id == 0 )
            {
                return;
            }


            var user_folders = this._OfficeXContext.GetFoldersByUserId( user_id );
            foreach( var folder in user_folders )
            {
                Int32 folder_id = 0;
                if( false == Int32.TryParse( folder.Value, out folder_id ) )
                {
                    continue;
                }

                var files = this._OfficeXContext.GetFilesByFolderId( folder_id );

                var private_files = files.Select( item => new ListItem( item.Text, FolderIDInfo.StrFromData( FolderIDInfo.eFolderType.Private, item.Value ) ) ).ToList();

                ucUserFolder ctrl = (ucUserFolder) Page.LoadControl( "~/UserControls/ucUserFolder.ascx" );

                ctrl.ToControls( folder.Text, folder.Value, private_files );

                this.panelMain.Controls.Add( ctrl );
            }

            var shared_folders = this._OfficeXContext.GetSharedFolders();
            foreach( var folder in shared_folders )
            {
                Int32 folder_id = 0;
                if( false == Int32.TryParse( folder.Value, out folder_id ) )
                {
                    continue;
                }

                var files = this._OfficeXContext.GetSharedFilesByFolderId( folder_id );
                var shared_files = files.Select( item => new ListItem( item.Text, FolderIDInfo.StrFromData( FolderIDInfo.eFolderType.Shared, item.Value ) ) ).ToList();

                ucUserFolder ctrl = (ucUserFolder) Page.LoadControl( "~/UserControls/ucUserFolder.ascx" );
                ctrl.ToControls( folder.Text, folder.Value, shared_files );

                this.panelMain.Controls.Add( ctrl );
            }

        }
    }
}