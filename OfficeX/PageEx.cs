﻿using OfficeXDataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeXDataModel.Interfaces;
using OfficeXDataModel;
using System.Diagnostics;

namespace OfficeX
{
    public static class PageEx
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        public static void CheckAutirize( this System.Web.UI.Page page )
        {
            if( page.IsPostBack == false )
            {
                if( page.IsConnect() == false )
                {
                    page.Response.Redirect( "~/Login.aspx" );
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static Boolean IsConnect( this System.Web.UI.Page page )
        {
            return ( 0 != page.GetCurrentUserID() );
        }

        /// <summary>
        /// Is Admin ?
        /// </summary>
        /// <param name="page"></param>
        /// <param name="IOfficeDBContext"></param>
        /// <returns></returns>
        public static Boolean IsAdmin( this System.Web.UI.Page page, IOfficeXDBContext iOfficeDBContext )
        {
            page.CheckAutirize();

            Int32 user_id = page.GetCurrentUserID();

            UserProfile user = iOfficeDBContext.GetUserByID( user_id );
 
            if( user == null )
            {
                page.Response.Redirect( "~/Login.aspx" );
            }

            return ( user.Status == UserProfile.eStatus.Admin );
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static Int32 GetCurrentUserID( this System.Web.UI.Page page )
        {
            try
            {
                Object id_obj = page.Session["MainUserID"];
                if( id_obj != null )
                {
                    if( id_obj is Int32 )
                    {
                        return (Int32) id_obj;
                    }
                }

            }
            catch( Exception ex )
            {
                Debug.WriteLine( String.Format( "Error:{0}", ex.Message ) );
            }

            return 0;
        }
    }
}