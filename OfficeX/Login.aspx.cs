﻿using OfficeXDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OfficeXDataModel.Model;

namespace OfficeX
{
    public partial class Login: System.Web.UI.Page
    {
        private OfficeXContext _OfficeXContext = new OfficeXContext();

        protected void Page_Load( object sender, EventArgs e )
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnValidateUser( object sender, EventArgs e )
        {
            if( String.IsNullOrWhiteSpace( this.ctrlLogin.UserName ) )
            {
                return;
            }

            UserProfile user_profile = _OfficeXContext.GetUserByEmail( this.ctrlLogin.UserName.Trim() );
            if( user_profile != null )
            {
                if( String.Compare( user_profile.Password, this.ctrlLogin.Password ) == 0 )
                {
                    Session["MainUserID"] = user_profile.Id;

                    this.Response.Redirect( "~/Default.aspx" );
                }
            }
        }
    }
}