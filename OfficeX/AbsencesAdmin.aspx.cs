﻿using OfficeXDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX
{
    public partial class AbsencesAdmin: System.Web.UI.Page
    {

        private OfficeXContext _OfficeXContext = new OfficeXContext();

        protected void Page_Load( object sender, EventArgs e )
        {

            this.CheckAutirize();

            if( this.IsPostBack == false )
            {
                BindGridAbsences();                
            }
            else
            {

            }
        }

        /// <summary>
        /// Binding grid
        /// </summary>
        private void BindGridAbsences()
        {
            ObjectDataSource ods = new ObjectDataSource()
            {
                TypeName = "OfficeX.Models.AbsenceAdminItemsDataSource",
                SelectMethod = "SelectAll",
                EnablePaging = true,
                MaximumRowsParameterName = "maximumRows",
                StartRowIndexParameterName = "startRowIndex",
                SortParameterName = "sort",
                SelectCountMethod = "SelectCount"
            };

            this.gvAbsences.DataSource = ods;
            this.gvAbsences.DataBind();
        }

        protected void gvAbsences_PageIndexChanging( object sender, System.Web.UI.WebControls.GridViewPageEventArgs e )
        {
            this.gvAbsences.PageIndex = e.NewPageIndex;

            BindGridAbsences();
        }

        protected void gvX_RowDataBound( object sender, GridViewRowEventArgs e )
        {
            if( e.Row.RowType == DataControlRowType.DataRow )
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink( this.gvAbsences, "Select$" + e.Row.RowIndex );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRefused_Click( object sender, EventArgs e )
        {
            var ids = GetSelectadAbsences();
            if( ids.Count > 0 )
            {
                OfficeXDataModel.Model.AbsenceTask.eStatusAbsence new_status = OfficeXDataModel.Model.AbsenceTask.eStatusAbsence.Readed;
                this._OfficeXContext.AbsencesChangeStatus( ids, new_status );
                BindGridAbsences();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnApproved_Click( object sender, EventArgs e )
        {
            var ids = GetSelectadAbsences();
            if( ids.Count > 0 )
            {
                OfficeXDataModel.Model.AbsenceTask.eStatusAbsence new_status = OfficeXDataModel.Model.AbsenceTask.eStatusAbsence.Approved;
                this._OfficeXContext.AbsencesChangeStatus( ids, new_status );
                BindGridAbsences();

                Page.Response.Redirect( "~/Default.aspx" );
            }
        }

        /// <summary>
        /// Get selected IDs
        /// </summary>
        /// <returns></returns>
        private IReadOnlyCollection<Int32> GetSelectadAbsences()        
        {
            List<Int32> result = new List<Int32>();

            foreach( GridViewRow row in this.gvAbsences.Rows )
            {
                if( this.gvAbsences.DataKeys[row.RowIndex].Value is Int32 )
                {
                    CheckBox check_box = (CheckBox) row.FindControl( "cbSelected" );
                    if( check_box != null && check_box.Checked )
                    {
                        Int32 id = (Int32)this.gvAbsences.DataKeys[row.RowIndex].Value;
                        if( id != 0 )
                        {
                            result.Add( id );
                        }
                        else
                        {
                            //TODO: info
                        }
                    }
                }
            }

            return result;
        }
    }
}