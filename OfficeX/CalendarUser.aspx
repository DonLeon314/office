﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CalendarUser.aspx.cs" Inherits="OfficeX.CalendarUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%@ Register TagPrefix="uc" TagName="CalendarDay" Src="~/UserControls/ucCalendarDay.ascx" %>    
    <%@ Register TagPrefix="uc" TagName="ProjectTaskEditor" Src="~/UserControls/ucProjectTaskEditor.ascx" %>    
    <%@ Register TagPrefix="uc" TagName="AbsenceEditor" Src="~/UserControls/ucAbsenceEditor.ascx" %>    

    <style type="text/css">
            .request {
                background-color: yellow;
                color:black;
            }

            .approved {
                background-color:darkolivegreen;
                color:aliceblue;
            }

            .readed {
                background-color: darkred;
                color:white;
            }

    </style>

<asp:Calendar Class="Calendar" ID="Calendar" runat="server" OnDayRender="Calendar_DayRender" OnSelectionChanged="Calendar_SelectionChanged" Width="100%">
</asp:Calendar>
<uc:ProjectTaskEditor id="ucProjectTaskEditor" runat="server" />
<uc:AbsenceEditor id ="ucAbsenceEditor" runat="server" />

</asp:Content>

