﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OfficeX._Default"culture="auto" meta:resourcekey="PageDefault" uiculture="auto" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="jumbotron">
        <%--<h5><%: Title %></h5>--%>
        <%--<p class="lead"><%: Title %></p>        --%>
        <asp:Label ID="lTitle" runat="server" Text="Hola! " Font-Size="XX-Large" />
    </div>
    <asp:Panel runat="server">
        <b>Perfil</b>
        <asp:Button ID="btnProjects" PostBackUrl="~/MyProfile.aspx" runat="server" meta:resourcekey="btnProfile" class="btn btn-default" />
    </asp:Panel>    
    <br />
    <asp:Panel runat="server">
        <b>Calendario</b>
        <asp:Button ID="btnCalendar" PostBackUrl="~/CalendarUser.aspx" runat="server" meta:resourcekey="btnCalendar" class="btn btn-default" />
    </asp:Panel>    
    <br /> 
    <asp:Panel runat="server">
        <b>Gestor documental</b>
        <asp:Button ID="btnUserFiles" PostBackUrl="~/UserFiles.aspx" runat="server" meta:resourcekey="btnUserFiles" class="btn btn-default" />
    </asp:Panel>
    <br />
    <asp:Panel ID="panelStat" runat="server" >
        <b>Informes</b>
            <asp:Button ID="btnStatProjects" PostBackUrl="~/StatProjects.aspx" runat="server" meta:resourcekey="btnStatProjects" class="btn btn-default" />
    </asp:Panel>
    <br />
    <asp:Panel ID="panelAdmin" runat="server" >
        <b>Mantenimiento</b>
            <asp:Button ID="btnAdminUsers" PostBackUrl="~/AdminUsers.aspx" runat="server" meta:resourcekey="btnAdminUsers" class="btn btn-default" />
            <asp:Button ID="btnAdminProjects" PostBackUrl="~/AdminProjects.aspx" runat="server" meta:resourcekey="btnAdminProjects" class="btn btn-default" />
            <asp:Button ID="btnAdminAbsences" PostBackUrl="~/AbsencesAdmin.aspx" runat="server" meta:resourcekey="btnAdminAbsences" class="btn btn-default" />
            <asp:Button ID="btnAdminCalendario" PostBackUrl="~/AdminCalendarios.aspx" runat="server" meta:resourcekey="btnAdminCalendario" class="btn btn-default" />
            <asp:Button ID="Button1" PostBackUrl="~/AdminFiles.aspx" runat="server" meta:resourcekey="btnAdminFiles" class="btn btn-default" />
    </asp:Panel>
    
</asp:Content>
