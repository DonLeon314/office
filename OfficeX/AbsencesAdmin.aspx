﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="AbsencesAdmin.aspx.cs" Inherits="OfficeX.AbsencesAdmin" %>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <asp:GridView ID="gvAbsences" class="gridAbsencess" runat="server" AutoGenerateColumns="False" Width="100%" 
                                DataKeyNames="Id" AllowPaging="True" PageSize="5"                                                                
                                meta:resourcekey="gvAbsences" 
                                OnPageIndexChanging="gvAbsences_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbSelected" runat="server" AutoPostBack="false"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Source" meta:resourcekey="ColumnSource"/>
                            <asp:BoundField DataField="StartDate" meta:resourcekey="ColumnData1"/>
                            <asp:BoundField DataField="EndDate" meta:resourcekey="ColumnData2"/>
                            <asp:BoundField DataField="TypeAbsence" meta:resourcekey="ColumnDay"/>
                            <asp:BoundField DataField="Description" meta:resourcekey="ColumnDescription"/>
                        </Columns>
                        <SelectedRowStyle ForeColor="White" Font-Bold="True" BackColor="#CE5D5A">
                        </SelectedRowStyle>
     </asp:GridView><br />
    <asp:Button ID="btnRefused" runat="server" style="padding:10px" class="btn btn-default" meta:resourcekey="btnRefused" OnClick="btnRefused_Click"  />
    <asp:Button ID="btnApproved" runat="server" style="padding:10px" class="btn btn-default" meta:resourcekey="btnApproved" OnClick="btnApproved_Click" />

</asp:Content>
