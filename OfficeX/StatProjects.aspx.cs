﻿using OfficeXDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using OfficeX.Models;
using System.Diagnostics;
using OfficeX.Utils;

namespace OfficeX
{
    public partial class StatProjects: System.Web.UI.Page
    {

        private OfficeXContext _OfficeXContext = new OfficeXContext();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load( object sender, EventArgs e )
        {
            this.CheckAutirize();

            if( this.IsPostBack )
            {
            }
            else
            {
                RefreshDates();
                RefreshProjects();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshDates()
        {
            this.tbDateCulture.Text = "es"; // Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName;

            DateTime end_date = DateTime.Now;            
            DateTime start_date = end_date.AddDays( -7 );            ;

            this.tbStartDate.Text = start_date.ToShortDateString();
            this.tbEndDate.Text = end_date.ToShortDateString();
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshProjects()
        {
            this.ddlProjects.Items.Clear();

            var projects = this._OfficeXContext.GetProjects();

            projects.Select( item => new ListItem( item.Name, item.Id.ToString() ) ).
                     ToList().
                     ForEach( list_item => this.ddlProjects.Items.Add( list_item ) );
        }

        /// <summary>
        /// Fire Refresh
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRefresh_Click( object sender, EventArgs e )
        {            

            BindGridAbsences();
        }

        /// <summary>
        /// 
        /// </summary>
        protected void BindGridAbsences()
        {
            ObjectDataSource ods = new ObjectDataSource()
            {
                TypeName = "OfficeX.Models.StatProjectDataSource",
                SelectMethod = "SelectAll",
                EnablePaging = true,
                MaximumRowsParameterName = "maximumRows",
                StartRowIndexParameterName = "startRowIndex",
                SortParameterName = "sort",
                SelectCountMethod = "SelectCount"
            };

            ods.ObjectCreated += Ods_ObjectCreated;

            this.gvStat.DataSource = ods;
            this.gvStat.DataBind();


            this.btnSend.Visible = gvStat.Rows.Count > 0;
        }

        StatProjectDataSource _StatDataSource = null;
        
        /// <summary>
        /// 
        /// </summary>
        private DateTime? StartDate
        {
            get
            {
                DateTime start_date;                

                if( DateTime.TryParse( this.tbStartDate.Text, out start_date ) == false )
                {
                    //TODO: info
                    return null;
                }

                return start_date;
            }
        }

        private DateTime? EndDate
        {
            get
            {

                DateTime end_date;

                if( DateTime.TryParse( this.tbEndDate.Text, out end_date ) == false )
                {
                    //TODO: info
                    return null;
                }

                return end_date;

            }
        }

        private Int32 ProjectInfo
        {
            get
            {                
                ListItem project_select = this.ddlProjects.SelectedItem;
                if( project_select == null )
                {
                    //TODO: info
                    return 0;
                }

                Int32 project_id = 0;
                if( Int32.TryParse( project_select.Value, out project_id ) == false )
                {
                    //TODO: info
                    return 0;
                }

                return project_id;
            }
        }

        private String ProjectName
        {
            get
            {
                ListItem project_selected = this.ddlProjects.SelectedItem;
                if( project_selected == null )
                {
                    //TODO: info
                    return "";
                }
                
                return project_selected.Text;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ods_ObjectCreated( object sender, ObjectDataSourceEventArgs e )
        {

            this._StatDataSource = e.ObjectInstance as StatProjectDataSource;
            
            if( _StatDataSource == null )
            {
                return;
            }

            DateTime? start_date = this.StartDate;
            DateTime? end_date = this.EndDate;
            Int32 project_id = this.ProjectInfo;

            if( start_date.HasValue == false || end_date.HasValue == false || project_id == 0 )
            {
                //TODO: info
                return;
            }
            
            // Parameters
            _StatDataSource.ProjectId = project_id;            
           _StatDataSource.Date1 = start_date.Value;
           _StatDataSource.Date2 = end_date.Value;
           
           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvStat_SelectedIndexChanging( object sender, System.Web.UI.WebControls.GridViewPageEventArgs e )
        {
            this.gvStat.PageIndex = e.NewPageIndex;

            BindGridAbsences();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvStat_RowDataBound( object sender, GridViewRowEventArgs e )
        {


            switch( e.Row.RowType )
            {
                case DataControlRowType.DataRow:
                    {
                        break;
                    }
                case DataControlRowType.Footer:
                    {
                        e.Row.Cells[1].Text = String.Format( "Hours: {0:D2}:{1:D2}",
                                                              (Int32)this._StatDataSource.TotalHours.TotalHours,
                                                              this._StatDataSource.TotalHours.Minutes );
                        e.Row.Cells[0].Text = String.Format( "Total usuarios: {0}", this._StatDataSource.SelectCount() );

                        break;
                    }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvStat_DataBinding( object sender, EventArgs e )
        {
            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSend_Click( object sender, EventArgs e )
        {            

            DateTime? start_date = this.StartDate;
            DateTime? end_date = this.EndDate;
            Int32 project_id = this.ProjectInfo;

            if( start_date.HasValue == false || end_date.HasValue == false || project_id == 0 )
            {
                //TODO: info
                return;
            }

            String file_name = Server.MapPath( "MailTemplates" ) + @"\\" + "ProjectStat.html";
            
            ProjectStatReport.Send( file_name, project_id, this.ProjectName, start_date.Value, end_date.Value );

            return;
        }
    }
}