﻿using OfficeXDataModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX
{
    public partial class ChangeMyPassword: System.Web.UI.Page
    {
        private OfficeXContext _OfficeXContext = new OfficeXContext();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load( object sender, EventArgs e )
        {
            this.CheckAutirize();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnChangePassword_Click( object sender, EventArgs e )
        {
            String old_password = this.tbOldPassword.Text.Trim();
            String new_password = this.tbNewPassword.Text.Trim();
            String confirm_new_password = this.tbConfigmNewPassword.Text.Trim();

            if( String.Compare( new_password, confirm_new_password, false ) == 0 )
            {
                Int32 user_id = this.GetCurrentUserID();
                if( user_id != 0 )
                {
                    var result = this._OfficeXContext.ChangePassword( user_id, old_password, new_password );
                    
                    Debug.WriteLine( String.Format( "Change password: {0}", result ) );

                    if( result )
                    {
                        this.Response.Redirect( "~/Default.aspx" );
                    }
                }
            }
        }
    }
}