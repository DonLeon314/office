﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminCalendarios.aspx.cs" Inherits="OfficeX.AdminCalendarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
            .request {
                background-color: yellow;
                color:black;
            }

            .approved {
                background-color:darkolivegreen;
                color:aliceblue;
            }

            .readed {
                background-color: darkred;
                color:white;
            }

    </style>
    <br />
    <asp:Panel ID="panel" runat="server">
        <asp:Label Text="Usuario:" runat="server" /><asp:DropDownList ID="ddlUsers" runat="server" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
        <br />
        <asp:Calendar Class="Calendar" ID="Calendar" runat="server" Width="100%" OnDayRender="Calendar_DayRender">
        </asp:Calendar>
    </asp:Panel>

</asp:Content>
