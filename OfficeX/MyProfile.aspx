﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MyProfile.aspx.cs" Inherits="OfficeX.MyProfile" culture="auto" meta:resourcekey="PageMyProfile" uiculture="auto" %>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">    
    <%@ Register TagPrefix="uc" TagName="UserProfile" Src="~/UserControls/ucUserProfile.ascx" %>    
    <%@ Register TagPrefix="uc" TagName="ChangePassword" Src="~/UserControls/ucChangePassword.ascx" %>    
    <h1><%: Title %></h1>
    <br />    
    <asp:Panel ID="panelUserProfile" runat="server">
        <uc:UserProfile id="ucProfileEditor" runat="server" />
        <asp:Button ID="btnChangePassword" runat="server" meta:resourcekey="btnChangePassword" class="btn btn-default" OnClick="btnChangePassword_Click" /> 
        <asp:Button ID="btnSaveDown" runat="server" meta:resourcekey="btnSave" class="btn btn-default" OnClick="btnSaveUpDown_Click" />
    </asp:Panel>    
    <asp:Panel ID="panelChangePassword" runat="server">
        <uc:ChangePassword ID="ChangePassword" runat="server" />        
    </asp:Panel>
</asp:Content>
