﻿using OfficeXDataModel.Interfaces;
using OfficeXDataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using static OfficeXDataModel.Model.AbsenceTask;

namespace OfficeX.Models
{
    public class AbsenceTaskSource
    {

        public Int32 Id
        {
            get;
            private set;
        }

        public Int32 UserProfileId
        {
            get;
            private set;
        }

        public DateTime StartDate
        {
            get;
            private set;
        }
                
        public DateTime? EndDate
        {
            get;
            private set;
        } 

        public String Description
        {
            get;
            private set;
        }

        public eStatusAbsence Status
        {
            get;
            private set;
        }

        public Int32 AbsenceTypeId
        {
            get;
            private set;
        }

        public IReadOnlyCollection<ListItem> AbsenceTypes
        {
            get;
            private set;
        }


        private AbsenceTaskSource()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileId"></param>
        /// <param name="date"></param>
        /// <param name="iOfficeXDBContext"></param>
        /// <returns></returns>
        public static AbsenceTaskSource Create( Int32 userProfileId, DateTime date, IOfficeXDBContext iOfficeXDBContext )
        {
            AbsenceTaskSource result = new AbsenceTaskSource()
            {
                Id = 0,
                UserProfileId = userProfileId,
                StartDate = date,
                AbsenceTypes = iOfficeXDBContext.GetAbsenceTypes()
            };

            return result;
        }

        /// <summary>
        /// Create source
        /// </summary>
        /// <param name="idAbsence"></param>
        /// <returns></returns>
        public static AbsenceTaskSource Create( Int32 idAbsence, IOfficeXDBContext iOfficeXDBContext )
        {
            AbsenceTaskSource result = new AbsenceTaskSource() { Id = idAbsence };

            
            AbsenceTask task = iOfficeXDBContext.GetAbsenceTaskById( idAbsence );
            result.Id = task.Id;
            result.StartDate = task.StartDate;
            result.EndDate = task.EndDate;
            result.Description = task.Description;
            result.Status = task.Status;
            result.UserProfileId = task.UserProfileId;
            
            result.AbsenceTypes = iOfficeXDBContext.GetAbsenceTypes();

            return result;

        }

        /// <summary>
        /// Default initialize
        /// </summary>
        private static void InitDefault()
        {
            //TODO: no impliment
        }
    }
}