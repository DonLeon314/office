﻿using System;
using System.Collections.ObjectModel;
using System.Web.UI.WebControls;

namespace OfficeXDataModel.Utils
{
    /// <summary>
    /// 
    /// </summary>
    internal class ListItemKeyedCollection:
            KeyedCollection<Int32,ListItem>
    {
        /// <summary>
        /// 
        /// </summary>
        public ListItemKeyedCollection()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override int GetKeyForItem( ListItem item )
        {
            Int32 id = 0;
            if( Int32.TryParse( item.Value, out id ) )            
            {
                return id;
            }

            throw new InvalidCastException();
        }
    }
}
