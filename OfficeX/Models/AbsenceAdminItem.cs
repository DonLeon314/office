﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OfficeX.Models
{
    public class AbsenceAdminItem
    {
        public Int32 Id
        {
            get;
            private set;
        }

        public String Source
        { 
            get;
            private set; 
        }
        
        public String StartDate
        {
            get;
            private set;
        }

        public String EndDate
        {
            get;
            private set;
        }
        
        public String TypeAbsence
        {
            get;
            private set;
        }

        public String Description
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="source"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="dayParte"></param>
        /// <param name="description"></param>
        public AbsenceAdminItem( Int32 id, String source, DateTime startDate, DateTime? endDate, String typeAbsence, String description )
        {
            this.Id = id;
            this.Source = source;
            this.StartDate = startDate.ToShortDateString();
            this.EndDate = ( endDate.HasValue ? endDate.Value.ToShortDateString() : this.StartDate );
            this.TypeAbsence = typeAbsence;
            this.Description = description;
        }
    }
}