﻿using OfficeXDataModel;
using OfficeXDataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OfficeX.Models
{
    public class ProjectsDataSource
    {
        private Int32 _Count = 0;
        private OfficeXContext _OfficeXContext = new OfficeXContext();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="maximumRows"></param>
        /// <param name="startRowIndex"></param>
        /// <returns></returns>
        public List<Project> SelectAll( Object sort, Int32 maximumRows, Int32 startRowIndex )
        {               

            this._Count = 0;

            var projects = this._OfficeXContext.GetProjects();

            this._Count = projects.Count;

            var result = projects.Skip( startRowIndex ).Take( maximumRows ).ToList();
            
            return result;
            
        }

        /// <summary>
        /// Get count rows
        /// </summary>
        /// <returns></returns>
        public Int32 SelectCount()
        {
            return this._Count;
        }
    }
}