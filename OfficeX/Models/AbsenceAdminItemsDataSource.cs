﻿using OfficeXDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OfficeX.Models
{
    public class AbsenceAdminItemsDataSource
    {
        private Int32 _Count = 0;
        private OfficeXContext _OfficeXContext = new OfficeXContext();

        /// <summary>
        /// 
        /// </summary>
        public AbsenceAdminItemsDataSource()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="maximumRows"></param>
        /// <param name="startRowIndex"></param>
        /// <returns></returns>
        public List<AbsenceAdminItem> SelectAll( Object sort, Int32 maximumRows, Int32 startRowIndex )
        {
            this._Count = 0;

            var absences = this._OfficeXContext.GetNewAbsences( DateTime.Now );

            this._Count = absences.Count;

            var result = absences.Skip( startRowIndex ).Take( maximumRows ).
                                Select( item => new AbsenceAdminItem( item.Id,
                                                                      item.UserProfile.Name + item.UserProfile.SurName,
                                                                      item.StartDate, 
                                                                      item.EndDate,
                                                                      item.AbsenceType.Name,
                                                                      item.Description ) ).
                                ToList();

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Int32 SelectCount()
        {
            return this._Count;
        }


    }

}