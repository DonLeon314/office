﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OfficeX.Models
{
    public class UCEventArgs:
            EventArgs
    {
        public Int32 Id
        {
            get;
            private set;
        }

        public UCEventArgs( Int32 id )
        {
            this.Id = id;
        }
    }
}