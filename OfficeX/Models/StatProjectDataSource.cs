﻿using OfficeXDataModel;
using OfficeXDataModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OfficeX.Models
{
    public class StatProjectDataSource
    {

        private Int32 _Count = 0;
        private OfficeXContext _OfficeXContext = new OfficeXContext();


        public TimeSpan TotalHours
        {
            get;
            private set;
        }


        public DateTime Date1
        {
            set;
            get;
        }

        public DateTime Date2
        {
            set;
            get;
        }

        public DateTime StartDate
        {
            get
            {
                return ( this.Date1 < this.Date2 ? this.Date1 : this.Date2 );
            }
        }

        public DateTime EndDate
        {
            get
            {
                return ( this.Date1 > this.Date2 ? this.Date1 : this.Date2 );
            }
        }

        public Int32 ProjectId
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="maximumRows"></param>
        /// <param name="startRowIndex"></param>
        /// <returns></returns>
        public List<StatProjectsItem> SelectAll( Object sort, Int32 maximumRows, Int32 startRowIndex )
        {
            var result = SelectAll();                       

            return result.Skip( startRowIndex ).Take( maximumRows ).ToList();

        }

        /// <summary>
        /// 
        /// </summary>
        public IProjectContextDataSource ProjectContext
        {
            get
            {
                return (
                            this.ProjectId == 0 ? 
                                                    null :
                                                    this._OfficeXContext.GetProjectByID( this.ProjectId )
                       );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<StatProjectsItem> SelectAll()
        {
            this._Count = 0;
            
            var result = this._OfficeXContext.GetStatProjects( this.ProjectId, this.StartDate, this.EndDate );

            this._Count = result.Count;

            this.TotalHours = TimeSpan.FromTicks( result.Sum( item => item.Hours.Ticks ) );

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Int32 SelectCount()
        {
            return this._Count;
        }
    }
}