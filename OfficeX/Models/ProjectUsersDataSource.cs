﻿using OfficeXDataModel;
using OfficeXDataModel.Model;
using OfficeXDataModel.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace OfficeX.Models
{
    public class ProjectUsersDataSource
    {

        private OfficeXContext _OfficeXContext = new OfficeXContext();

        /// <summary>
        /// Constructor
        /// </summary>
        public ProjectUsersDataSource()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IReadOnlyCollection<ListItem> ProjectUsers( Int32 projectId, Int32 parentUserProfileId )
        {
            IReadOnlyCollection<ListItem> result = this._OfficeXContext.GetChildUsers( parentUserProfileId, true );
            
            var project_users = this._OfficeXContext.GetProjectUsers( projectId );

            foreach( int id in project_users )
            {
                String id_str = id.ToString();
                
                ListItem user_in_project = result.Where( item => String.Compare( id_str, item.Value ) == 0 ).FirstOrDefault();

                user_in_project.Selected = ( user_in_project != null );
            }

            return result;
        }
    }
}