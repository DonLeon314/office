﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using OfficeXDataModel.Interfaces;

namespace OfficeX.Models
{
    public class ProjectTaskDataSource:
            IProjectTaskDataSource
    {
        /// <summary>
        /// Constructor (NEW PROJECT TASK)
        /// </summary>
        /// <param name="userProfileID"></param>
        /// <param name="date"></param>
        private ProjectTaskDataSource( Int32 userProfileID, DateTime date )
        {            
            this.UserProfileID = userProfileID;
            this.Date = date.Date;
        }

        private ProjectTaskDataSource()
        {
            
        }

        public static ProjectTaskDataSource CreateNewProjectTaskDataSource( Int32 userProfileID, DateTime date, IOfficeXDBContext iOfficeXDBContext )
        {
            ProjectTaskDataSource result = new ProjectTaskDataSource( userProfileID, date.Date );

            result.Projects = iOfficeXDBContext.GetProjectsByUserID( userProfileID );

            if( result.Projects.Count > 0 )
            {
                var first = result.Projects.First();
                Int32 id = 0;
                if( Int32.TryParse( first.Value, out id ) )
                {
                    result.ProjectID = id;
                }
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileID"></param>
        /// <param name="date"></param>
        /// <param name="iOfficeXDBContext"></param>
        /// <returns></returns>
        public static ProjectTaskDataSource CreateNewProjectTaskDataSource( Int32 userProfileID, Int32 projectTaskId, IOfficeXDBContext iOfficeXDBContext )
        {

            var task = iOfficeXDBContext.GetProjectTaskById( projectTaskId );

            ProjectTaskDataSource result = new ProjectTaskDataSource()
            {
                ProjectTaskID = task.Id,
                UserProfileID = task.UserProfileId,
                ProjectID = task.ProjectId,
                Hours = task.Hours,
                Description = task.Description,
                Date = task.Date,
                Projects = iOfficeXDBContext.GetProjectsByUserID( userProfileID )
            };

            return result;
        }


        #region interface IProjectTaskDataSource

        public Int32 ProjectTaskID
        {
            get;
            private set;
        }

        public Int32 UserProfileID
        {
            get;
            private set;
        }

        public Int32 ProjectID
        {
            get;
            private set;
        }

        public TimeSpan Hours
        {
            get;
            private set;
        }

        public String Description
        {
            get;
            private set;
        }

        public DateTime Date
        {
            get;
            private set;
        }

        public IReadOnlyCollection<ListItem> Projects
        {
            get;
            private set;
        }
        #endregion

    }
}