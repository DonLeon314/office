﻿using OfficeXDataModel;
using OfficeXDataModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OfficeX.Models
{
    public class UserProfilesDataSource
    {
        private OfficeXContext _OfficeXContext = new OfficeXContext();
        private Int32 _Count = 0;

        public Int32 ParentUserProfileId
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="maximumRows"></param>
        /// <param name="startRowIndex"></param>
        /// <returns></returns>
        public List<IUserProfileContextDataSource> SelectAll( Object sort, Int32 maximumRows, Int32 startRowIndex )
        {
            var result = SelectAll();

            return result.Skip( startRowIndex ).Take( maximumRows ).ToList();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IReadOnlyCollection<IUserProfileContextDataSource> SelectAll()
        {
            this._Count = 0;

            var result = this._OfficeXContext.GetChildUsersInfo( this.ParentUserProfileId, false );

            this._Count = result.Count;
            
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Int32 SelectCount()
        {
            return this._Count;
        }
    }
}