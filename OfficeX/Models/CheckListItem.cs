﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OfficeX.Models
{
    public class CheckListItem
    {
        public Boolean Selected
        {
            get;
            set;
        }

        public String Text
        {
            get;
            set;
        }

        public String Value
        {
            get;
            private set;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="text"></param>
        /// <param name="value"></param>
        /// <param name="selected"></param>
        public CheckListItem( String text, String value, Boolean selected = false )
        {
            this.Text = text;
            this.Value = value;
            this.Selected = selected;
        }
    }
}