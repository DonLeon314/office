﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StatProjects.aspx.cs" Inherits="OfficeX.StatProjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script type="text/javascript" src="Scripts/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="Scripts/jQuery-UI/jquery-ui.js"></script>
    <script type="text/javascript" src="Scripts/jQuery-UI/i18n/datepicker-es.js"></script>
    <script type="text/javascript" src="Scripts/jQuery-UI/i18n/datepicker-ru.js"></script>
    <script type="text/javascript" src="Scripts/Office/StatProjects.js"></script>

    <br />
    <asp:Label Text="Projectos:" runat="server"></asp:Label>
    <asp:DropDownList ID="ddlProjects" runat="server" Width="100%">
    </asp:DropDownList><br/><br/>
    <asp:Label Text="Fecha inicio:" runat="server" ></asp:Label>
    <asp:TextBox ID="tbStartDate" ClientID="tbStartDate" runat="server"></asp:TextBox>
    <asp:Label Text="Fecha fin:" runat="server"></asp:Label>
    <asp:TextBox ID="tbEndDate" ClientID="tbEndDate" runat="server"></asp:TextBox><br /><br />
    <asp:Button ID="btnRefresh" runat="server" Text="Actualizar" OnClick="btnRefresh_Click" /><br /><br />
    <asp:GridView ID="gvStat" runat="server" PageSize="2" Width="100%" AutoGenerateColumns="False"
            AllowPaging="True" OnPageIndexChanging="gvStat_SelectedIndexChanging" OnDataBinding="gvStat_DataBinding" 
            OnRowDataBound="gvStat_RowDataBound" ShowFooter="True">
        <Columns>
            <asp:BoundField DataField="UserName" HeaderText="Usuario" />
            <asp:BoundField DataField="Hours" HeaderText="Horas" DataFormatString="{0:hh\:mm}" />
        </Columns>
    </asp:GridView>
    <asp:Button ID="btnSend" Visible="false" runat="server" Text="Enviar" OnClick="btnSend_Click" />
    <asp:TextBox ID="tbDateCulture" ClientID="tbDateCulture" runat="server" Style="display: none;" />
</asp:Content>
