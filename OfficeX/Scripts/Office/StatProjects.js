﻿$(function () {
    var region = $("[ClientID='tbDateCulture']").val();

    $.datepicker.setDefaults($.extend($.datepicker.regional[region /*'es'*/]));

    $("[ClientID='tbStartDate']").datepicker({
        showOn: 'button',
        buttonText: '...'
    });


    $("[ClientID='tbEndDate']").datepicker({
        showOn: 'button',
        buttonText: '...'
    });

}
);