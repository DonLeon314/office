﻿


    //-------------------------------------------------------------------------------------------------

    $(function ()
        {
            var region = $("[ClientID='tbDateCulture']").val();

            $.datepicker.setDefaults( $.extend( $.datepicker.regional[ region /*'es'*/ ] ) );

            $("[ClientID='tbStartDatePicker']").datepicker({
                            showOn: 'button',
                            buttonText: '...' });
        

            $("[ClientID='tbEndDatePicker']").datepicker({
                            showOn: 'button',
                            buttonText: '...' });                

        }
    );

    //-------------------------------------------------------------------------------------------------

    function OnChangeDuration()
    {
        var AbsenceTypesCtrl = $("[ClientID='ddlAbsenceTypes']");
        var PanelEndDatePickerCtrl = $("[ClientID='panelEndDatePicker']");

        var value = AbsenceTypesCtrl.val();
        if (value != undefined && value.length > 0)
        {
            var tokens = value.split(',');
            if (tokens.length == 2)
            {
                var duration = tokens[1];
                if (duration != undefined && duration.trim().length > 0)
                {
                    if (/AllDay/.test(duration))
                    {
                        PanelEndDatePickerCtrl.show();
                    }
                    else
                    {
                        PanelEndDatePickerCtrl.hide();
                        $("[ClientID='tbEndDatePicker']").val('');
                    }
                }
            }
        }
    }

    //-------------------------------------------------------------------------------------------------
