﻿$(function () {
    var region = $("[ClientID='tbDateCulture']").val();

    $.datepicker.setDefaults($.extend($.datepicker.regional[region /*'es'*/]));

    $("[ClientID='tbBirthDay']").datepicker({
        showOn: 'button',
        buttonText: '...'
    });


    $("[ClientID='tbContractDate']").datepicker({
        showOn: 'button',
        buttonText: '...'
    });

}
);