﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangeMyPassword.aspx.cs" Inherits="OfficeX.ChangeMyPassword" meta:resourcekey="PageResource1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1><%: Title %></h1>
    <br />
    <div style="justify-content:center">
        <asp:Label runat="server" meta:resourcekey="lOldPassword"></asp:Label><asp:TextBox ID="tbOldPassword" runat="server"></asp:TextBox><br />
        <asp:Label runat="server" meta:resourcekey="lNewPassword"></asp:Label><asp:TextBox ID="tbNewPassword" runat="server"></asp:TextBox><br />
        <asp:Label runat="server" meta:resourcekey="lConfirmNewPassword"></asp:Label><asp:TextBox ID="tbConfigmNewPassword" runat="server"></asp:TextBox><br /><br />
        <asp:Button ID="btnChangePassword" runat="server" meta:resourcekey="btnChangePassword" class="btn btn-default" OnClick="btnChangePassword_Click"/>
        <asp:Button ID="btnCancel" runat="server"  PostBackUrl="~/Default.aspx" meta:resourcekey="btnCancel" class="btn btn-default" />
        <br />
    </div>
</asp:Content>
