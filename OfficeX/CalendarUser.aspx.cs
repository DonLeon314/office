﻿using OfficeX.Models;
using OfficeX.UserControls;
using OfficeXDataModel;
using OfficeXDataModel.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX
{
    public partial class CalendarUser: System.Web.UI.Page
    {

        private OfficeXContext _OfficeXContext = new OfficeXContext();

        protected void Page_Load( object sender, EventArgs e )
        {

            this.CheckAutirize();

            if( this.IsPostBack == false )
            {

                ShowProjectTaskEditor( false );
                ShowAbsenceEditor( false );

            }
            else
            {
                DayPostBackFilter();                
            }

            this.ucProjectTaskEditor.OnOKClick += UcProjectTaskEditor_OnOKClick;
            this.ucProjectTaskEditor.OnCANCELClick += UcProjectTaskEditor_OnCANCELClick;

            this.ucAbsenceEditor.OnCANCELClick += UcAbsenceEditor_OnCANCELClick;
            this.ucAbsenceEditor.OnOKClick += UcAbsenceEditor_OnOKClick;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UcAbsenceEditor_OnOKClick( object sender, EventArgs e )
        {
            this._OfficeXContext.SaveAbsenceTask( this.ucAbsenceEditor );
            ShowAbsenceEditor( false );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UcAbsenceEditor_OnCANCELClick( object sender, EventArgs e )
        {
            ShowAbsenceEditor( false );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UcProjectTaskEditor_OnCANCELClick( object sender, EventArgs e )
        {
            ShowProjectTaskEditor( false );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UcProjectTaskEditor_OnOKClick( object sender, EventArgs e )
        {
            
            this._OfficeXContext.SaveProjectTask( this.ucProjectTaskEditor );

            ShowProjectTaskEditor( false );

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="showCtrl"></param>
        private void ShowProjectTaskEditor( Boolean showCtrl )
        {
//             if( showCtrl )
//             {
//                 this.Calendar.Width = new Unit( "50%" );
//                 this.ucProjectTaskEditor.Width = new Unit( "50%" );
//             }
//             else
//             {
//                 this.Calendar.Width = new Unit( "100%" );             
//             } 

            this.ucProjectTaskEditor.Visible = showCtrl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="showCtrl"></param>
        private void ShowAbsenceEditor( Boolean showCtrl )
        {
            
            this.ucAbsenceEditor.Visible = showCtrl;
        }

        /// <summary>
        /// 
        /// </summary>
        private void DayPostBackFilter()
        {
            if( Page.Form == null )
            {
                return;
            }

            String command = Request.Form["__EVENTTARGET"].Trim();
            if( String.IsNullOrWhiteSpace( command ) )
            {
                return; 
            }
            
            String parameters = Request.Form["__EVENTARGUMENT"].Trim();

            if( String.Compare( command, "AddProjectTask", true ) == 0 )
            {
                OnAddProjectTask();
            }
            else
            {
                if( String.Compare( command, "DeleteTasks" ) == 0 )
                {
                    OnDeleteTasks( parameters );
                }
                else
                {
                    if( String.Compare( command, "EditTask" ) == 0 )
                    {
                        OnEditTask( parameters );
                    }
                    else
                    {
                        if( String.Compare( command, "AddAbsenceTask" ) == 0 )
                        {
                            OnAddAbsenceTask();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnAddProjectTask()
        {
            Debug.WriteLine( "OnAddProjectTask" );

            ShowProjectTaskEditor( true );

            Int32 userId = Page.GetCurrentUserID();

            this.ucProjectTaskEditor.ToControls( ProjectTaskDataSource.CreateNewProjectTaskDataSource( userId, this.Calendar.SelectedDate, this._OfficeXContext ) );

            return;
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnAddAbsenceTask()
        {
            Debug.WriteLine( "OnAddAbsenceTask" );
            
            Int32 user_id = Page.GetCurrentUserID();

            if( user_id != 0 )
            {
                ShowAbsenceEditor( true );
                this.ucAbsenceEditor.ToControls( AbsenceTaskSource.Create( user_id, this.Calendar.SelectedDate, this._OfficeXContext ) );
            }

            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        private void OnEditTask( String parameters )
        {
            Debug.WriteLine( String.Format( "OnEditTask({0})", parameters ) );
            IReadOnlyCollection<TaskID> ids;
            if( TaskID.TryParse( parameters, out ids ) )
            {
                if( ids.Count > 0 )
                {                    
                    Int32 userId = Page.GetCurrentUserID();
                    TaskID task_id = ids.First();
                    if( task_id != null )
                    {
                        switch( task_id.TaskType )
                        {
                            case TaskID.eTaskEnum.eProjectTask:
                                {
                                    ShowProjectTaskEditor( true );
                                    this.ucProjectTaskEditor.ToControls( ProjectTaskDataSource.CreateNewProjectTaskDataSource( userId, task_id.ID, this._OfficeXContext ) );
                                    break;
                                }
                            case TaskID.eTaskEnum.eAusence:
                                {
                                    ShowAbsenceEditor( true );
                                    this.ucAbsenceEditor.ToControls( AbsenceTaskSource.Create( task_id.ID, this._OfficeXContext ) );
                                    break;
                                }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameters"></param>
        private void OnDeleteTasks( String parameters )
        {
            Debug.WriteLine( String.Format("OnDeleteTasks({0})", parameters ) );

            IReadOnlyCollection<TaskID> ids;
            if( TaskID.TryParse( parameters, out ids ) )
            {
                this._OfficeXContext.DeleteTasks( ids );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Calendar_SelectionChanged( object sender, EventArgs e )
        {
      

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Calendar_DayRender( object sender, DayRenderEventArgs e )
        {
            Calendar ctrl = (Calendar)sender;
            if( ctrl == null )
            {
                return;
            }

            Int32 user_id = Page.GetCurrentUserID();
            TaskBuilder tb = new TaskBuilder();

            var items = tb.Build( this._OfficeXContext, user_id, e.Day.Date );
            
            if( e.Day.Date == ctrl.SelectedDate )
            {
                ucCalendarDay day_ctrl = Page.LoadControl( "~/UserControls/ucCalendarDay.ascx" ) as ucCalendarDay;
                day_ctrl.ID = "ucCalendarDay";
                day_ctrl.ToControls( items );
                e.Cell.Controls.Add( day_ctrl );
            }
            else
            {   
                if( items.Count > 0 )
                {
                    ListBox list_box = new ListBox();

                    list_box.CssClass = "day_list";

                    list_box.Items.AddRange( items.ToArray() );
                                                            
                    e.Cell.Controls.Add( new LiteralControl( "<br />" ) );
                    e.Cell.Controls.Add( list_box );
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ucCalendarDay_Click( object sender, EventArgs e )
        {
            Debug.WriteLine( "Click" );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LinkButton1_Click( object sender, EventArgs e )
        {
            return;
        }
    }
}