﻿using OfficeXDataModel;
using System;
using System.Web.UI;
 
namespace OfficeX
{
    public partial class _Default: Page
    {
        private OfficeXContext _OfficeXContext = new OfficeXContext();

        protected void Page_Load( object sender, EventArgs e )
        {
            this.CheckAutirize();

            if( this.IsPostBack == false )
            {
                if( this.IsAdmin( this._OfficeXContext ) )
                {
                    this.panelAdmin.Visible = true;
                    this.panelStat.Visible = true;
//                     this.btnAdminUsers.Visible = true;
//                     this.btnAdminProjects.Visible = true;
//                     this.btnAdminAbsences.Visible = true;
//                     this.btnStatProjects.Visible = true;
                }
                else
                {
                    this.panelAdmin.Visible = false;
                    this.panelStat.Visible = false;
                    //                     this.btnAdminUsers.Visible = false;
                    //                     this.btnAdminProjects.Visible = false;
                    //                     this.btnAdminAbsences.Visible = false;
                    //                     this.btnStatProjects.Visible = false;
                }

                Int32 id = Page.GetCurrentUserID();
                String name = String.Empty;
                if( id != 0 )
                {
                    var user_info = this._OfficeXContext.GetUserProfileByID( id );
                    if( user_info != null )
                    {
                        name = user_info.Name.Trim();
                        if( name.Length > 0 )
                        {
                            name += " ";
                        }
                        name += user_info.SurName;

                        if( String.IsNullOrWhiteSpace( name ) )
                        {
                            name = user_info.EMail;
                        }
                    }
                }

                if( String.IsNullOrWhiteSpace( name ) )
                {
                    this.lTitle.Text = "Hola!";
                }
                else
                {
                    this.lTitle.Text = String.Format( "Hola, {0}!", name );
                }

            }
        }
    }
}