﻿using OfficeXDataModel;
using OfficeXDataModel.Interfaces;
using OfficeXDataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX
{
    public partial class MyProfile: System.Web.UI.Page
    {
        private OfficeXContext _OfficeXContext = new OfficeXContext();

        protected void Page_Load( object sender, EventArgs e )
        {
            this.CheckAutirize();


            if( this.IsPostBack == false )
            {

                Int32 current_user_id = this.GetCurrentUserID();

                IUserProfileContextDataSource user_profile = this._OfficeXContext.GetUserProfileByID( current_user_id );
                if( user_profile != null )
                {
                    this.ucProfileEditor.ToControls( user_profile, false );
                }

                this.panelChangePassword.Style["display"] = "none";
            }
            else
            {
                
            }

            this.ChangePassword.OnHide += ChangePassword_OnHide;
            this.ChangePassword.OnSave += ChangePassword_OnSave;

        }

        private void ChangePassword_OnSave( object sender, EventArgs e )
        {
            this._OfficeXContext.AdminChangePassword( this.ChangePassword.Id, this.ChangePassword.Password );

            this.panelChangePassword.Style["display"] = "none";
            this.panelUserProfile.Style["display"] = "";
        }

        private void ChangePassword_OnHide( object sender, EventArgs e )
        {
            this.panelChangePassword.Style["display"] = "none";
            this.panelUserProfile.Style["display"] = "";
        }

        protected void btnSaveUpDown_Click( object sender, EventArgs e )
        {
                                                            // EDIT
            this._OfficeXContext.SaveUser( this.ucProfileEditor, 0 );
        }

        protected void btnChangePassword_Click( object sender, EventArgs e )
        {
            this.panelChangePassword.Style["display"] = "";
            this.panelUserProfile.Style["display"] = "none";

            Int32 id = this.GetCurrentUserID();

            this.ChangePassword.ToControls( id );
        }               
    }
}