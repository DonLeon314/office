﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OfficeX.Utils
{
    public class FolderIDInfo
    {
        public enum eFolderType
        {
            Private,
            Shared
        }

        public Int32 BBDDId
        {
            get;
            private set;
        }

        public eFolderType FolderType
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bbddId"></param>
        /// <param name="folderType"></param>
        public FolderIDInfo( Int32 bbddId, eFolderType folderType )
        {
            this.BBDDId = bbddId;
            this.FolderType = folderType;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            String result = String.Format( "{0},{1}", this.FolderType.ToString(), this.BBDDId.ToString() );

            return result;
        }

        public static String StrFromData( eFolderType folderType, String strId )
        {
            return String.Format( "{0},{1}", folderType.ToString(), strId.Trim() );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strId"></param>
        /// <returns></returns>
        public static FolderIDInfo FromString( String strId )
        {
            if( String.IsNullOrWhiteSpace( strId ) )
            {
                return null;
            }

            String[] tokens = strId.Split( new Char[] { ',' } );

            if( tokens.Length > 1 )
            {
                Int32 id = 0;
                if( Int32.TryParse( tokens[1], out id ) )
                {
                    eFolderType folder_type = eFolderType.Private;
                    if( Enum.TryParse( tokens[0], out folder_type ) )
                    {
                        return new FolderIDInfo( id, folder_type );
                    }
                }
            }

            throw new InvalidCastException( String.Format( "Invalid cast '{0}' to FolderIDInfo ", strId ) );
        }
    }
}