﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace OfficeX.Utils
{
    public class SimpleTemplate
    {
        /// <summary>
        /// 
        /// </summary>
        Dictionary<String, List<String>> _Source = new Dictionary<String, List<String>>()
        { 
            { "<!--_HEADER_-->", new List<String>() },
            { "<!--_TABLE_ROW_-->", new List<String>() },
            { "<!--_FOOTER_-->", new List<String>() }
        };

        /// <summary>
        /// 
        /// </summary>
        public SimpleTemplate()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public Boolean Load( String fileName )
        {
            try
            {
                using( StreamReader reader = new StreamReader( fileName ) )
                {
                    String line = String.Empty;
                    List<String> parte = null;

                    while( ( line = reader.ReadLine() ) != null )
                    {
                        String key = line.Trim().ToUpper();
                        if( this._Source.ContainsKey( key ) )
                        {
                            parte = this._Source[key];
                            continue;
                        }

                        if( parte == null )
                        {
                            continue;
                        }

                        parte.Add( line );
                    }
                }

                return true;
            }
            catch (System.Exception ex)
            {
                //TODO: info
                Debug.WriteLine( String.Format( "Error:{0}", ex.Message ) );
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="allVariables"></param>
        /// <param name="rowsVariable"></param>
        /// <returns></returns>
        public String Generate( Dictionary<String, String> allVariables, IEnumerable<Dictionary<String, String>> rowsVariable )
        {
            StringBuilder result = new StringBuilder();

            foreach( String new_line in GenerateParte( this._Source["<!--_HEADER_-->"], allVariables ) )
            {
                result.Append( new_line );
            }

            foreach( var row_vars in rowsVariable )
            {
                foreach( String new_line in GenerateParte( this._Source["<!--_TABLE_ROW_-->"], row_vars ) )
                {
                    result.Append( new_line );
                }
            }

            foreach( String new_line in GenerateParte( this._Source["<!--_FOOTER_-->"], allVariables ) )
            {
                result.Append( new_line );
            }                        
            
            return result.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateParte"></param>
        /// <param name="variables"></param>
        /// <returns></returns>
        public IEnumerable<String> GenerateParte( List<String> templateParte, Dictionary<String, String> variables )
        {
            foreach( String template_line in templateParte )
            {
                String new_line = template_line;
                foreach( var variable in variables )
                {
                    new_line = new_line.Replace( variable.Key, variable.Value );
                }

                yield return new_line;
            }
        }

    }
}