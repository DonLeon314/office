﻿using OfficeX.Models;
using OfficeXDataModel.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace OfficeX.Utils
{
    public class ProjectStatReport
    {

        public static void Send( String templateFileName, Int32 projectId, String projectName, DateTime date1, DateTime date2 )
        {
            String file_name = String.Empty;

            try
            {
                SimpleTemplate send_content = new SimpleTemplate();
                
                if( send_content.Load( templateFileName ) )
                {
                    Debug.WriteLine( "Load template OK!" );
                }

                // generate variables
                StatProjectDataSource data_source = new StatProjectDataSource()
                {
                    ProjectId = projectId
                };

                data_source.Date1 = date1;
                data_source.Date2 = date2;
                
                var rows = data_source.SelectAll();
                var project_context = data_source.ProjectContext;

                if( rows.Count == 0 || project_context == null || String.IsNullOrWhiteSpace( project_context.ProjectContactEMail ) )
                {
                    //TODO: info
                    return;
                }

                Dictionary<String, String> variables = new Dictionary<String, String>();                
                variables.Add( "_PROJECT_NAME_", project_context.Name );
                variables.Add( "_START_DATE_", data_source.StartDate.ToShortDateString() );
                variables.Add( "_END_DATE_", data_source.EndDate.ToShortDateString() );
                variables.Add( "_PROJECT_CONTACT_NAME_", project_context.ProjectContactName );
                                
                variables.Add( "_TOTAL_USERS_", data_source.SelectCount().ToString() );
                variables.Add( "_TOTAL_TIME_", String.Format( "{0:D2}:{1:D2}",
                                                             (Int32)data_source.TotalHours.TotalHours,
                                                              data_source.TotalHours.Minutes ) );


                List<Dictionary<String, String>> variables_row = new List<Dictionary<String, String>>();
                foreach( var row in rows )
                {
                    Dictionary<String, String> row_vars = new Dictionary<String, String>();

                    row_vars.Add( "_COLUMN_1_", row.UserName );
                    row_vars.Add( "_COLUMN_2_", String.Format("{0:D2}:{1:D2}", row.Hours.Hours, row.Hours.Minutes ) );

                    variables_row.Add( row_vars );
                }

                String report_content = send_content.Generate( variables, variables_row );

                if( String.IsNullOrWhiteSpace( report_content ) )
                {
                    return;
                }               

                SMTPMailInformator informator = SMTPMailInformator.Create();
                informator.OnSend( null, new InformatorArgs( project_context.ProjectContactEMail, report_content, true,
                                                             String.Format("Informe de horas del proyecto:{0}", projectName ) ) );

            }
            catch( Exception ex )
            {
                //TODO: info
                Debug.WriteLine( String.Format( "Error:{0}", ex.Message ) );
            }

        }
    }
}