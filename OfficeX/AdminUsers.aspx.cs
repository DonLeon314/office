﻿using OfficeX.Models;
using OfficeXDataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX
{
    public partial class AdminUsers: System.Web.UI.Page
    {

        private OfficeXContext _OfficeXContext = new OfficeXContext();

        protected void Page_Load( object sender, EventArgs e )
        {
            this.CheckAutirize();

            if( this.IsPostBack )
            {
            }
            else
            {                
                RefreshUserProfiles();

                this.panelUserEditor.Visible = false;
                this.panelChangePassword.Visible = false;
            }

            this.ucChangePassword.OnSave += UcChangePassword_OnSave;
            this.ucChangePassword.OnHide += UcChangePassword_OnHide;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UcChangePassword_OnHide( object sender, EventArgs e )
        {
            this.panelChangePassword.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UcChangePassword_OnSave( object sender, EventArgs e )
        {
            Int32 user_profile_id = this.ucChangePassword.Id;
            if( user_profile_id != 0 )
            {
                this._OfficeXContext.AdminChangePassword( user_profile_id, this.ucChangePassword.Password );
                this.panelChangePassword.Visible = false;                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshUserProfiles()
        {
            ObjectDataSource ods = new ObjectDataSource()
            {
                TypeName = "OfficeX.Models.UserProfilesDataSource",
                SelectMethod = "SelectAll",
                EnablePaging = true,
                MaximumRowsParameterName = "maximumRows",
                StartRowIndexParameterName = "startRowIndex",
                SortParameterName = "sort",
                SelectCountMethod = "SelectCount"
            };

            ods.ObjectCreated += Ods_ObjectCreated;

            this.gvUserProfiles.DataSource = ods;
            this.gvUserProfiles.DataBind();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ods_ObjectCreated( object sender, ObjectDataSourceEventArgs e )
        {
            UserProfilesDataSource ds = e.ObjectInstance as UserProfilesDataSource;
            if( ds == null )
            {
                return;
            }

            Int32 id = this.GetCurrentUserID();
            if( id != 0 )
            {
                ds.ParentUserProfileId = id;
            }            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUserProfiles_RowDataBound( object sender, GridViewRowEventArgs e )
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUserProfiles_PageIndexChanging( object sender, GridViewPageEventArgs e )
        {
            this.gvUserProfiles.PageIndex = e.NewPageIndex;

            RefreshUserProfiles();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvUserProfiles_RowCommand( object sender, GridViewCommandEventArgs e )
        {
            if( String.Compare( e.CommandName, "edit_row", true ) == 0 )
            {
                OnEdit( Argument2Id( e.CommandArgument ) );
            }
            else
            {
                if( String.Compare( e.CommandName, "delete_row", true ) == 0 )
                {
                    OnDelete( Argument2Id( e.CommandArgument ) );
                }
                else
                {
                    if( String.Compare( e.CommandName, "change_password", true ) == 0 )
                    {
                        OnChangePassword( Argument2Id( e.CommandArgument ) );
                    }
                }
            }
            return;
        }

        private void OnChangePassword( Int32 userProfileId )
        {
            if( userProfileId != 0 )
            {
                this.panelChangePassword.Visible = true;
                this.ucChangePassword.ToControls( userProfileId );                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userProfileId"></param>
        private void OnEdit( Int32 userProfileId )
        {
            if( userProfileId != 0 )
            {
                var data = this._OfficeXContext.GetUserProfileByID( userProfileId );

                if( data != null )
                {
                    this.panelUserEditor.Visible = true;
                    this.ucProfileEditor.ToControls( data, true );
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objArgument"></param>
        /// <returns></returns>
        private Int32 Argument2Id( Object objArgument )
        {
            Int32 result = 0;
            Int32 index = 0;

            if( Int32.TryParse( (String)objArgument, out index ) )
            {
                result = (Int32) this.gvUserProfiles.DataKeys[index].Value;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        private void OnDelete( Int32 userProfileId )
        {
            if( userProfileId != 0 )
            {
                this._OfficeXContext.DeleteUserProfileById( userProfileId );
                RefreshUserProfiles();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click( object sender, EventArgs e )
        {
            this._OfficeXContext.SaveUser( this.ucProfileEditor, this.GetCurrentUserID() );
            RefreshUserProfiles();
            this.panelUserEditor.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnHide_Click( object sender, EventArgs e )
        {
            this.panelUserEditor.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ntnAddUserProfile_Click( object sender, EventArgs e )
        {
            this.panelUserEditor.Visible = true;
            this.ucProfileEditor.ToControls( null, true );
        }
    }
}