﻿    using OfficeX.Models;
using OfficeXDataModel;
using OfficeXDataModel.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OfficeX
{
    /// <summary>
    /// 
    /// </summary>
    public partial class AdminProjects: System.Web.UI.Page
    {

        /// <summary>
        /// 
        /// </summary>
        private OfficeXContext _OfficeXContext = new OfficeXContext();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load( object sender, EventArgs e )
        {
            this.CheckAutirize();

            if( this.IsPostBack == false )
            {
                BindGridProjects();

                this.ucProjectEditor.Visible = false;                
            }
            else
            {
                
            }

            this.ucProjectEditor.OnOKClick += UcProjectEditor_OnOKClick;
            this.ucProjectEditor.OnCANCELClick += UcProjectEditor_OnCANCELClick;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UcProjectEditor_OnCANCELClick( object sender, EventArgs e )
        {
            this.ucProjectEditor.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UcProjectEditor_OnOKClick( object sender, EventArgs e )
        {
            this._OfficeXContext.SaveProject( this.ucProjectEditor );

            BindGridProjects();

            this.ucProjectEditor.Visible = false;
        }

        /// <summary>
        /// Binding grid
        /// </summary>
        private void BindGridProjects()
        {
            ObjectDataSource ods = new ObjectDataSource()
            {
                TypeName = "OfficeX.Models.ProjectsDataSource",
                SelectMethod = "SelectAll",
                EnablePaging = true,
                MaximumRowsParameterName = "maximumRows",
                StartRowIndexParameterName = "startRowIndex",
                SortParameterName = "sort",
                SelectCountMethod = "SelectCount"
            };
            
            this.gvProjects.DataSource = ods;
            this.gvProjects.DataBind();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvProjects_PageIndexChanging( object sender, System.Web.UI.WebControls.GridViewPageEventArgs e )
        {
            this.gvProjects.PageIndex = e.NewPageIndex;

            BindGridProjects();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvProjects_RowCommand( object sender, System.Web.UI.WebControls.GridViewCommandEventArgs e )
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvX_RowDataBound( object sender, GridViewRowEventArgs e )
        {
            if( e.Row.RowType == DataControlRowType.DataRow )
            {
                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink( this.gvProjects, "Select$" + e.Row.RowIndex );                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvProjects_SelectedIndexChanged( object sender, EventArgs e )
        {
            Int32 user_id = this.GetCurrentUserID();
            if( user_id == 0 )
            {
                return; //TODO: error
            }

            Object selected = this.gvProjects.SelectedValue;

            if( selected != null && selected is Int32 )
            {
                Int32 id_project = (Int32)selected;
                if( id_project != 0 )
                {
                    ProjectUsersDataSource ds = new ProjectUsersDataSource();

                    this.ucProjectEditor.Visible = true;
                    this.ucProjectEditor.ToControls( this._OfficeXContext.GetProjectByID( id_project ), ds.ProjectUsers( id_project, user_id ) );
                }
            }

            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddProject_Click( object sender, EventArgs e )
        {
            this.ucProjectEditor.Visible = true;

            var users = this._OfficeXContext.GetChildUsers( Page.GetCurrentUserID(), true );
            this.ucProjectEditor.ToControls( null, users );

            BindGridProjects();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void btnDeleteProject_Click(object sender, EventArgs e)
        {
            Object selected = this.gvProjects.SelectedValue;

            if (selected != null && selected is Int32)
            {
                Int32 id_project = (Int32)selected;
                if (id_project != 0)
                {
                    this._OfficeXContext.DeleteProjectById(id_project);
                    BindGridProjects();
                }
            }
        }

        //[WebMethod]
        //public void DeleteProject_Click()
        //{
        //    Object selected = this.gvProjects.SelectedValue;

        //    if (selected != null && selected is Int32)
        //    {
        //        Int32 id_project = (Int32)selected;
        //        if (id_project != 0)
        //        {
        //            this._OfficeXContext.DeleteProjectById(id_project);
        //            BindGridProjects();
        //        }
        //    }
        //}

    }
}