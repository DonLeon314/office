﻿using OfficeXDataModel;
using OfficeXDataModel.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.IO;
using OfficeX.Utils;

namespace OfficeX
{
    public partial class AdminFiles: System.Web.UI.Page
    {

        private OfficeXContext _OfficeXContext = new OfficeXContext();

        protected void Page_Load( object sender, EventArgs e )
        {
            this.CheckAutirize();

            if( this.IsPostBack == false )
            {
                if( this.IsAdmin( this._OfficeXContext ) == false )
                {
                    Page.Response.Redirect( "~/Default.aspx" );
                }
                RefreshUsers();
            }
//             else
//             {
//                 RefreshUsers();
//             }
        }

        /// <summary>
        /// Refresh Users
        /// </summary>
        private void RefreshUsers()
        {
            Int32 my_id = this.GetCurrentUserID();

            var users = this._OfficeXContext.GetChildUsers( my_id, true );
            this.ddlUsers.Items.Clear();
            users.ForEach( item =>
            {
                this.ddlUsers.Items.Add( item );
            } );

            if( users.Count != 0 )
            {
                RefreshFolders( this.ddlUsers.SelectedValue = users.First().Value );
            }
            else
            {
                this.ddlFolders.Items.Clear();
                this.lbFiles.Items.Clear();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlUsers_SelectedIndexChanged( object sender, EventArgs e )
        {
            ListItem selected = this.ddlUsers.SelectedItem;
            if( selected != null )
            {
                RefreshFolders( selected.Value );
            }
            else
            {
                this.ddlFolders.Items.Clear();
                this.lbFiles.Items.Clear();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userValue"></param>
        private void RefreshFolders( String userValue )
        {
            Int32 id_user = 0;
            if( false == Int32.TryParse( userValue, out id_user ) || id_user == 0 )
            {
                return;
            }

            List<ListItem> all_folders = new List<ListItem>();

            var folders = this._OfficeXContext.GetFoldersByUserId( id_user );
            all_folders.AddRange( folders.Select( item => new ListItem( item.Text,
                                                                        FolderIDInfo.StrFromData( Utils.FolderIDInfo.eFolderType.Private, item.Value ) ) ).
                                          ToList() );

            var shared_folders = this._OfficeXContext.GetSharedFolders();
            all_folders.AddRange( shared_folders.Select( item => new ListItem( item.Text,
                                                                               FolderIDInfo.StrFromData(FolderIDInfo.eFolderType.Shared, item.Value )             
                                                                             ) ).ToList() );

            this.ddlFolders.Items.Clear();
            all_folders.ForEach( item => this.ddlFolders.Items.Add( item ) );
            if( all_folders.Count != 0 )
            {
                RefreshFiles( this.ddlFolders.SelectedValue = all_folders.First().Value );
            }
            else
            {
                this.lbFiles.Items.Clear();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderValue"></param>
        private void RefreshFiles( String folderValue )
        {
            IReadOnlyCollection<ListItem> files = new List<ListItem>();

            FolderIDInfo folder_id_info = FolderIDInfo.FromString( folderValue );

            if( folder_id_info != null )
            {
                switch( folder_id_info.FolderType )
                {
                    case FolderIDInfo.eFolderType.Private:
                        {
                            files = PrivateFiles( folder_id_info.BBDDId );     
                            break;
                        }
                    case FolderIDInfo.eFolderType.Shared:
                        {
                            files = SharedFiles( folder_id_info.BBDDId );
                            break;
                        }
                }                
            }
            this.lbFiles.Items.Clear();
            files.ForEach( item => this.lbFiles.Items.Add( item ) );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        private IReadOnlyCollection<ListItem> SharedFiles( Int32 folderId )
        {   
            var files_raw = this._OfficeXContext.GetSharedFilesByFolderId( folderId );

            return files_raw.Select( item => new ListItem( item.Text,
                                                           FolderIDInfo.StrFromData( FolderIDInfo.eFolderType.Shared, item.Value ) ) ).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        private IReadOnlyCollection<ListItem> PrivateFiles( Int32 folderId )
        {
            var files_raw = this._OfficeXContext.GetFilesByFolderId( folderId );

            return files_raw.Select( item => new ListItem( item.Text,
                                                           FolderIDInfo.StrFromData( FolderIDInfo.eFolderType.Private, item.Value ) ) ).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpload_Click( object sender, EventArgs e )
        {
            ListItem selected_user = this.ddlUsers.SelectedItem;
            ListItem selected_folder = this.ddlFolders.SelectedItem;

            if( selected_user == null || selected_folder == null )
            {
                return;
            }

            Int32 id_user = 0;
            FolderIDInfo folder_id_info = FolderIDInfo.FromString(selected_folder.Value);

            if( folder_id_info == null || Int32.TryParse( selected_user.Value, out id_user ) == false || id_user == 0 )
            {
                return;
            }
            
            var file_data = this.fuFile.FileBytes;
            if( file_data == null || file_data.Length == 0 )
            {
                return;
            }

            if( SaveFile( id_user, folder_id_info, file_data, this.fuFile.FileName ) )
            {
                switch( folder_id_info.FolderType )
                {
                    case FolderIDInfo.eFolderType.Private:
                        {
                            this._OfficeXContext.SaveFileToUserFolder( folder_id_info.BBDDId, this.fuFile.FileName );
                            break;
                        }
                    case FolderIDInfo.eFolderType.Shared:
                        {
                            this._OfficeXContext.SaveFileToSharedFolder( folder_id_info.BBDDId, this.fuFile.FileName );
                            break;
                        }
                }
            }

            RefreshFiles( selected_folder.Value );

            return;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="folderId"></param>
        /// <param name="dataFile"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private Boolean SaveFile( Int32 userId, FolderIDInfo folderInfo, byte[] dataFile, String fileName )
        {
            try
            {
                String path = String.Empty;

                switch( folderInfo.FolderType )
                {
                    case FolderIDInfo.eFolderType.Private:
                        {
                            path = Server.MapPath( "Files" );
                            path = path + @"\" + userId.ToString() + @"\" + folderInfo.BBDDId.ToString();

                            break;
                        }
                    case FolderIDInfo.eFolderType.Shared:
                        {
                            path = Server.MapPath( "SharedFiles" );
                            path = path + @"\" + folderInfo.BBDDId.ToString();
                            break;
                        }
                }

                if( false == Directory.Exists( path ) )
                {
                    Directory.CreateDirectory( path );
                }

                String file_name = path + @"\" + fileName;

                using( var fs = new FileStream( file_name, FileMode.OpenOrCreate, FileAccess.Write ) )
                {
                    fs.SetLength( 0 );
                    fs.Write( dataFile, 0, dataFile.Length );                    
                }

                return true;
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine( String.Format( "Error:{0}", ex.Message ) );	
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlFolders_SelectedIndexChanged( object sender, EventArgs e )
        {
            ListItem selected = this.ddlFolders.SelectedItem;
            if( selected != null )
            {
                RefreshFiles( selected.Value );
            }
            else
            {
                this.lbFiles.Items.Clear();
            }            

            return;
        }

        protected void btnDownload_Click( object sender, EventArgs e )
        {
            ListItem selected_user = this.ddlUsers.SelectedItem;
            ListItem selected_folder = this.ddlFolders.SelectedItem;
            ListItem selected_file = this.lbFiles.SelectedItem;

            if( selected_user == null || selected_folder == null || selected_file == null )
            {
                return;
            }

            Int32 id_user = 0;
            FolderIDInfo folder_id_info = FolderIDInfo.FromString( selected_folder.Value );

            if( Int32.TryParse( selected_user.Value, out id_user ) == false || 
                folder_id_info == null || id_user == 0 )
            {
                return;
            }

            String url = String.Empty;
            switch( folder_id_info.FolderType )
            {
                case FolderIDInfo.eFolderType.Private:
                    {
                        url = @"Files/" + id_user.ToString() + @"/" + folder_id_info.BBDDId.ToString() + @"/" + selected_file.Text;
                        break;
                    }

                case FolderIDInfo.eFolderType.Shared:
                    {
                        url = @"SharedFiles/" + folder_id_info.BBDDId.ToString() + @"/" + selected_file.Text;
                        break;
                    }
            }

            if( String.IsNullOrWhiteSpace( url ) == false )
            {
                Page.Response.Redirect( url );
            }

        }
    }
}