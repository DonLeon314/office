﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminUsers.aspx.cs" Inherits="OfficeX.AdminUsers" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">    
    
    <%@ Register TagPrefix="uc" TagName="UserProfile" Src="~/UserControls/ucUserProfile.ascx" %>    
    <%@ Register TagPrefix="uc" TagName="ChangePassword" Src="~/UserControls/ucChangePassword.ascx" %>    

    <asp:Button ID="ntnAddUserProfile" runat="server" Text="Add user" meta:resourcekey="ntnAddUserProfileResource1" OnClick="ntnAddUserProfile_Click" /><br />
    <asp:GridView ID="gvUserProfiles" runat="server" PageSize="10" Width="100%" AutoGenerateColumns="False"
            AllowPaging="True" meta:resourcekey="gvUserProfilesResource1" OnPageIndexChanging="gvUserProfiles_PageIndexChanging" 
            OnRowDataBound="gvUserProfiles_RowDataBound" OnRowCommand="gvUserProfiles_RowCommand"
            DataKeyNames="Id" >
        <Columns>
            <asp:BoundField DataField="Id" Visible="false" ReadOnly="True" />
            <asp:ButtonField meta:resourcekey="rowBtnEdit" CommandName="edit_row" />
            <asp:ButtonField meta:resourcekey="rowBtnDel" CommandName="delete_row" />
            <asp:ButtonField meta:resourcekey="rowBtnPassword" CommandName="change_password" />
            <asp:BoundField DataField="Name" meta:resourcekey="BoundFieldResource1"/>
            <asp:BoundField DataField="SurName" meta:resourcekey="BoundFieldResource2"/>
            <asp:BoundField DataField="EMail" meta:resourcekey="BoundFieldResource3"/>
        </Columns>
    </asp:GridView>
    <asp:Panel ID="panelUserEditor" runat="server" meta:resourcekey="panelUserEditorResource1">
        <div>
            <uc:UserProfile ID="ucProfileEditor" runat="server" />
        </div>
        <asp:Button ID="btnSave" runat="server" meta:resourcekey="btnSave" class="btn btn-default" OnClick="btnSave_Click" />
        <asp:Button ID="btnHide" runat="server" meta:resourcekey="btnHide" class="btn btn-default" OnClick="btnHide_Click" />
    </asp:Panel>
    <asp:Panel ID="panelChangePassword" runat="server" meta:resourcekey="panelChangePassword">
        <uc:ChangePassword ID="ucChangePassword" runat="server" />        
    </asp:Panel>
</asp:Content>
