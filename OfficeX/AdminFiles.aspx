﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminFiles.aspx.cs" Inherits="OfficeX.AdminFiles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<br />
<asp:Label Text="Usuarios:" runat="server" />
<asp:DropDownList ID="ddlUsers" runat="server" Width="100%" OnSelectedIndexChanged="ddlUsers_SelectedIndexChanged" AutoPostBack="True" ></asp:DropDownList><br /><br />
<asp:Label Text="Carpetas:" runat="server" />
<asp:DropDownList ID="ddlFolders" runat="server" Width="100%" OnSelectedIndexChanged="ddlFolders_SelectedIndexChanged" AutoPostBack="True" ></asp:DropDownList><br />
<asp:Label Text="Archivos:" runat="server" />
<asp:ListBox ID="lbFiles" runat="server" Width="100%"></asp:ListBox>
<br />
<asp:FileUpload ID="fuFile" runat="server" Width="100%" />
<asp:Button ID="btnUpload" runat="server" Text="Cargar" Width="60px" OnClick="btnUpload_Click" />


</asp:Content>
