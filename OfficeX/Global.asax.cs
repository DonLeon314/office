﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Threading;

namespace OfficeX
{
    public class Global: HttpApplication
    {
        void Application_Start( object sender, EventArgs e )
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes( RouteTable.Routes );
            BundleConfig.RegisterBundles( BundleTable.Bundles );


            RouteTable.Routes.MapHttpRoute( name: "DefaultApi",
                                            routeTemplate: "api/{controller}/{id}",
                                            defaults: new
                                            {
                                                id = System.Web.Http.RouteParameter.Optional
                                            } );
        }

        void Application_AcquireRequestState( object sender, EventArgs e )
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo( "es-Es" );
            Thread.CurrentThread.CurrentCulture = new CultureInfo( "es-Es" );
        }

    }
}