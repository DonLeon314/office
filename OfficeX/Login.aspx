﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="OfficeX.Login" culture="auto" meta:resourcekey="PageResource" uiculture="auto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<br/>
    <h2><%: Title %></h2>
    <br />
    <div style="display:flex; justify-content:center">
       <asp:Login ID = "ctrlLogin" runat = "server" OnAuthenticate= "OnValidateUser" meta:resourcekey="ctrlLoginResource" ></asp:Login>
    </div>
</asp:Content>
